<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class TestController extends Controller {
    public function index() 
    {
        echo "<b>User Information: </b>";
        var_dump(get_quiz_user());
        echo "<br/>";
        
        /* Get Mac Address */
        ob_start();
        system('ipconfig/all');
        $mycom = ob_get_contents();
        ob_clean();
        
        $findme = "Physical";
        $pmac   = strpos($mycom, $findme); // Find the position of Physical text
        $mac    = substr($mycom,($pmac+36),17); // Get Physical Address
        echo "<b>Mac Address: </b>" . $mac . "<br/>";
        /* Get Mac Address */
        
        return response()->view('test');
    }

}
