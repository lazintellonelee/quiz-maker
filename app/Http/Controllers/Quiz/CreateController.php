<?php

namespace App\Http\Controllers\Quiz;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Quiz;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Grade;

class CreateController extends Controller 
{
    public function index()
    {
        DB::beginTransaction();
        return response()->view('create-quiz');
    }
    
    public function create(Request $request)
    {
        $data = $request->input();
        
        DB::beginTransaction();
        
        $db_quiz = Quiz::create(array(
            'quiz_title'        => $data['quiz_title'],
            'quiz_category'     => $data['quiz_category'],
            'quiz_force'        => $data['quiz_force'],
            'quiz_time_out'     => $data['time_out']!==''?$data['time_out']:null,
            'quiz_one_time'     => $data['one_time']==='true'?'1':'0',
            'quiz_created_by'   => 0,
            'quiz_secret_key'   => md5(uniqid())
        ));
        
        if(!$db_quiz)
        {
            echo json_encode(array(
                'error' => 'Problems with database connect! Please try again'
            ));
            exit();
        }
        
        $questions  = $data['questions'];
        foreach($questions as $question)
        {
            $db_questions = Question::create(array(
                'quiz_id'           => $db_quiz->quiz_id,
                'question_content'  => $question['content'],
                'question_type'     => $question['type'],
                'question_display'  => $question['display'],
                'question_index'    => $question['index']
            ));
            
            if(!$db_questions)
            {
                DB::rollBack();
                echo json_encode(array(
                    'error' => 'Problems with database connect! Please try again'
                ));
                exit();
            }
            
            $answers = $question['answers'];
            foreach($answers as $answer)
            {
                if(!Answer::create(array(
                    'question_id'       => $db_questions->question_id,
                    'answer_content'    => $answer['content'],
                    'answer_score'      => $answer['score'],
                    'answer_index'      => $answer['index']
                )))
                {
                    DB::rollBack();
                    echo json_encode(array(
                        'error' => 'Problems with database connect! Please try again'
                    ));
                    exit();
                }
            }
        }
        
        DB::commit();
        echo json_encode(array(
            'quiz_id'       => $db_quiz->quiz_id,
            'quiz_embed'    => url('public/js/embed/quiz-maker.quiz-embed.js?q=' . $db_quiz->quiz_id . ';sk=' . $db_quiz->quiz_secret_key . ';bootstrap=1')
        ));
        exit();
    }
    
    public function createGrades(Request $request) 
    {
        $data       = $request->input('grades');
        
        $quiz_id    = array_get(current($data), 'quiz_id');
        $json       = array();
        if(!Grade::insert($data))
        {
            $json['error'] = 'Problems with database connect! Please try again';
        }
        $quiz = Quiz::where('quiz_id', $quiz_id)
                ->first(array(
                    'quiz_id',
                    'quiz_title',
                    'quiz_secret_key'
                ));
        
        $json['quiz_embed'] = url('public/js/embed/quiz-maker.quiz-embed.js?q=' . $quiz['quiz_id'] . ';sk=' . $quiz['quiz_secret_key'] . ';bootstrap=1');
        
        echo json_encode($json);
        exit();
    }
}