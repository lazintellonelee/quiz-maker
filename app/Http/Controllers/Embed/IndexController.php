<?php

namespace App\Http\Controllers\Embed;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Quiz;
use App\Models\Question;
use App\Models\Answer;

class IndexController extends Controller 
{
    public function index()
    {
        return response()->view('embed-test');
    }
    
    public function getQuiz(Request $request)
    {
        if(isset($_SERVER['HTTP_ORIGIN']))
        {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true ");
            header("Access-Control-Allow-Methods: POST");
        }
        
        $data   =   $request->input();
        $result =   array();
        $quiz   =   Quiz::where('quiz_id', $data['quiz_id'])
                    ->where('quiz_secret_key', $data['quiz_secret_key'])
                    ->first(array(
                        'quiz_id',
                        'quiz_title',
                        'quiz_type',
                        'quiz_force',
                        'quiz_time_out',
                    ));
        
        if(!$quiz)
        {
            echo json_encode(array(
                'no_quiz' => 1
            ));
            exit();
        }
        
        $result['quiz'] = $quiz->toArray();
        
        $questions      =   Question::where('quiz_id', $quiz->quiz_id)
                            ->orderBy('question_index', 'asc')
                            ->get(array(
                                'question_id',
                                'question_content',
                                'question_type',
                                'question_display'
                            ));
                            
        if($questions)
        {
            $questions = $questions->toArray();
            foreach($questions as $key => $value)
            {
                $answers =  Answer::where('question_id', $value['question_id'])
                            ->orderBy('answer_index', 'asc')
                            ->get(array(
                                'answer_id',
                                'answer_content',
                                'answer_index'
                            ));
                
                if($answers)
                {
                    $questions[$key]['answers'] = $answers->toArray();
                }
            }
            $result['questions'] = $questions;
        }
        
        echo json_encode($result);
        exit();
    }
    
    public function getResult(Request $request)
    {
        if(isset($_SERVER['HTTP_ORIGIN']))
        {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header("Access-Control-Allow-Credentials: true ");
            header("Access-Control-Allow-Methods: POST");
        }
        
        $data = $request->input();
        
        $quiz = Quiz::where('quiz_id', $data['quiz_id'])->first()->toArray();
        
        echo json_encode(array(
            'score'     => Answer::GetScoreAnswers(explode(',', $data['answers'])),
            'max_score' => Answer::GetMaxScore(explode(',', $data['questions']))
        ));
        exit();
    }
}