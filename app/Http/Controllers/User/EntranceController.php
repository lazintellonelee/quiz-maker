<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;

class EntranceController extends Controller 
{
    public function signUp(Request $request)
    {
        $data = $request->input();
        if(User::where('user_email', $data['user_email'])->first())
        {
            return response()->json(array(
                'error' => array(
                    'Email has been used.'
                )
            ));
        }
        
        $data['user_salt']      = md5($data['user_password'] . uniqid());
        $data['user_password']  = md5($data['user_password'] . $data['user_salt']);
        
        $user = User::create($data);
        if($user) {
            return response()->json(array(
                'user_id'       => $user->user_id,
                'user_name'     => $user->user_name,
                'user_email'    => $user->user_email
            ))->withCookie(cookie()->forever('quiz_user', array(
                'user_id'       => $user->user_id,
                'user_name'     => $user->user_name,
                'user_email'    => $user->user_email
            )));
        }
    }
    
    public function logIn(Request $request)
    {
        $data = $request->input();
        $user = User::where('user_email', $data['user_email'])->first();
        if(!$user)
        {
            return response()->json(array(
                'error' => array(
                    'Email does not exist.'
                )
            ));
        }
        if($user->user_password !== md5($data['user_password'] . $user->user_salt))
        {
            return response()->json(array(
                'error' => array(
                    'Password incorrect.'
                )
            ));
        }
        return response()->json(array(
            'user_id'       => $user->user_id,
            'user_name'     => $user->user_name,
            'user_email'    => $user->user_email
        ))->withCookie(cookie()->forever('quiz_user', array(
            'user_id'       => $user->user_id,
            'user_name'     => $user->user_name,
            'user_email'    => $user->user_email
        )));
    }
    
    public function signOut()
    {
        return redirect()->route('home')->withCookie(
            cookie('quiz_user', null)
        );
    }
}