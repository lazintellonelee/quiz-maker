<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/test.html', array(
    'as'    => 'test',
    'uses'  => 'TestController@index'
));

Route::get('/', array(
    'as'    => 'home',
    'uses'  => 'Home\IndexController@index'
));

Route::get('/create-quiz.html', array(
    'as'            => 'create-quiz',
    'uses'          => 'Quiz\CreateController@index',
    'middleware'    => 'logined'
));

Route::post('/create-quiz.html', array(
    'as'    => 'process-create-quiz',
    'uses'  => 'Quiz\CreateController@create'
));

Route::post('/create-grades.html', array(
    'as'    => 'process-create-grades',
    'uses'  => 'Quiz\CreateController@createGrades'
));

Route::get('/embed-test.html', array(
    'as'    => 'embed-test',
    'uses'  => 'Embed\IndexController@index'
));

Route::post('/embed-get-quiz.html', array(
    'as'    => 'embed-get-quiz',
    'uses'  => 'Embed\IndexController@getQuiz'
));

Route::post('/embed-get-result.html', array(
    'as'    => 'embed-get-result',
    'uses'  => 'Embed\IndexController@getResult'
));

Route::post('/sign-up.html', array(
    'as'    => 'sign-up',
    'uses'  => 'User\EntranceController@signUp'
));

Route::post('/log-in.html', array(
    'as'    => 'log-in',
    'uses'  => 'User\EntranceController@logIn'
));

Route::get('/sign-out.html', array(
    'as'    => 'sign-out',
    'uses'  => 'User\EntranceController@signOut'
));
