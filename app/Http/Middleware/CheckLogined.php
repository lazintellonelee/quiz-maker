<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Models\Admin;

class CheckLogined {

    public function handle($request, Closure $next) {
        if(get_quiz_user())
        {
            return $next($request);
        }

        return redirect()->route('home');
    }
}
    