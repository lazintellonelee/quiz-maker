<?php
 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Question;

class Answer extends Model 
{
    protected $table        = 'tbl_answers';
    protected $primaryKey   = 'answer_id';
    protected $fillable     = array(
        'question_id',
        'answer_content',
        'answer_score',
        'answer_index',
    );
            
    public $timestamps      = true;
    
    public static function GetMaxScore($questionsId)
    {
        $max_score = 0;
        foreach($questionsId as $question_id)
        {
            $question   = Question::where('question_id', $question_id)->first()->toArray();
            $answers    = Answer::where('question_id', $question_id)->get()->toArray();
            $score      = 0;
            foreach($answers as $answer)
            {
                if($question['question_type'] === "multi_choice")
                {
                    $score = $score + $answer['answer_score'];
                    continue;
                }
                if($answer['answer_score'] > $score)
                {
                    $score = $answer['answer_score'];
                }
            }
            $max_score = $max_score + $score;
        }
        return $max_score;
    }
    
    public static function GetScoreAnswers($answersID)
    {
        $answers    = self::whereIn('answer_id', $answersID)->get()->toArray();
        $score      = 0;
        foreach($answers as $answer)
        {
            $score  = $score + $answer['answer_score'];
        }
        return $score;
    }
}
