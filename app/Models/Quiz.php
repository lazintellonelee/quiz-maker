<?php
 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model 
{
    protected $table        = 'tbl_quizes';
    protected $primaryKey   = 'quiz_id';
    protected $fillable     = array(
        'quiz_title',
        'quiz_category',
        'quiz_force',
        'quiz_time_out',
        'quiz_one_time',
        'quiz_created_by',
        'quiz_secret_key'
    );
            
    public $timestamps      = true;
}
