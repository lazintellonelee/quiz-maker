<?php
 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model 
{
    protected $table        = 'tbl_users';
    protected $primaryKey   = 'user_id';
    protected $fillable     = array(
        'user_email',
        'user_name',
        'user_password',
        'user_salt'
    );
            
    public $timestamps      = true;
}
