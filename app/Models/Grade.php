<?php
 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model 
{
    protected $table        = 'tbl_grades';
    protected $primaryKey   = 'grade_id';
    protected $fillable     = array(
        'quiz_id',
        'grade_title',
        'grade_description',
        'grade_type',
        'grade_from',
        'grade_to'
    );
            
    public $timestamps      = true;
}
