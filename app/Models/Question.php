<?php
 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model 
{
    protected $table        = 'tbl_questions';
    protected $primaryKey   = 'question_id';
    protected $fillable     = array(
        'quiz_id',
        'question_content',
        'question_type',
        'question_display',
        'question_index'
    );
            
    public $timestamps      = true;
    
}
