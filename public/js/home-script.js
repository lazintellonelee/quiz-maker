$(document).ready(function () {
    $('#sign-up').on('click', function (event) {
        event.preventDefault();
        $('#modal-signup').modal('show');
        return true;
    });
    
    $('#modal-signup').on('hidden.bs.modal', function(event){
        $('#error-content').addClass('hidden');
        document.getElementById('form-sign-up').reset();
        return true;
    });

    $('#change-action-signup').on('click', function (event) {
        var action = $(this).attr('data-action');
        if (action === "login")
        {
            $(this).html('Sign up');
            $('#signup-retype-email').parent('div').fadeOut();
            $('#signup-username').parent('div').fadeOut();
            $('#signup-button').html('Log In');
            $(this).attr('data-action', 'signup');
            return true;
        }
        $(this).html('Login');
        $('#signup-retype-email').parent('div').fadeIn();
        $('#signup-username').parent('div').fadeIn();
        $('#signup-button').html('Sign Up');
        $(this).attr('data-action', 'login');
        return true;
    });
    
    $(document).on('click', '#signup-button', function(event){
        $('#error-content').addClass('hidden');
        var action  = $('#change-action-signup').attr('data-action')==='login'?'signup':'login';
        
        if(action === "signup" && $('#signup-retype-email').val() !== $('#signup-email').val())
        {
            $('#error-content')
            .removeClass('hidden')
            .children('div.alert').html("<strong>Error!</strong> Retype email not match.");
            return true;
        }
        
        var url     = $('#form-sign-up').attr('data-' + action);
        
        var form    = new FormData(document.getElementById('form-sign-up'));
        var http    = new XMLHttpRequest();
        http.open('POST', url, true);
        http.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
        http.onload = function(event)
        {
            var result = JSON.parse(this.responseText);
            if(result.error !== undefined) 
            {
                $('#error-content')
                .removeClass('hidden')
                .children('div.alert').html($.map(result.error, function(error){
                    return "<strong>Error!</strong> " + error + "<br/>";
                }));
                return true;
            }
            $('#sign-up').parents('li').addClass('hidden');
            $('#nav-user-name').html(result.user_name).parents('li').removeClass('hidden');
            $('#modal-signup').modal('hide');
            return true;
        };
        http.send(form);
    });
    
    window.onbeforeunload = function(event)
    {
        //
    };
});