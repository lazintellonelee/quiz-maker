var Grades = function()
{
    this.max_score  = 0;
    this.grades = [
    ];
    
    this.GetGrades = function()
    {
        return this.grades;
    };
    
    this.GetPostGrades = function() 
    {
        var grades = [];
        for(var i=0; i<this.grades.length; i++)
        {
            grades[i] = {
                quiz_id             : this.grades[i].quiz_id,
                grade_title         : this.grades[i].grade_title,
                grade_description   : this.grades[i].grade_description,
                grade_type          : this.grades[i].grade_type,
                grade_from          : this.grades[i].grade_from,
                grade_to            : this.grades[i].grade_to
            };
        }
        return grades;
    };
    
    this.SetMaxScore = function(score)
    {
        this.max_score = score;
        return true;
    };
    
    this.AddGrade = function()
    {
        var len     = this.grades.length;
        var grade   = {
            index               : len===0?0:(this.grades[len-1].index + 1),
            grade_title         : '',
            grade_description   : '',
            grade_type          : 'score',
            grade_from          : null,
            grade_to            : null
        };
        this.grades[len] = grade;
        return grade;
    };
    
    this.RemoveGrade = function(index)
    {
        for(var i=0; i<this.grades.length; i++)
        {
            if(this.grades[i].index === index)
            {
                this.grades.splice(i, 1);
                return true;
            }
        }
        return true;
    };
    
    this.SetGradeTitle = function(index, title)
    {
        for(var i=0; i<this.grades.length; i++)
        {
            if(this.grades[i].index === index)
            {
                this.grades[i].grade_title = title;
                return true;
            }
        }
        return true;
    };
    
    this.SetGradeDescription = function(index, description)
    {
        for(var i=0; i<this.grades.length; i++)
        {
            if(this.grades[i].index === index)
            {
                this.grades[i].grade_description = description;
                return true;
            }
        }
        return true;
    };
    
    this.SetGradeType = function(index, type)
    {
        for(var i=0; i<this.grades.length; i++)
        {
            if(this.grades[i].index === index)
            {
                this.grades[i].grade_type = type;
                return true;
            }
        }
        return true;
    };
    
    this.SetGradeFrom = function(index, from)
    {
        for(var i=0; i<this.grades.length; i++)
        {
            if(this.grades[i].index === index)
            {
                this.grades[i].grade_from = from;
                return true;
            }
        }
        return true;
    };
    
    this.SetGradeTo = function(index, to)
    {
        for(var i=0; i<this.grades.length; i++)
        {
            if(this.grades[i].index === index)
            {
                this.grades[i].grade_to = to;
                return true;
            }
        }
        return true;
    };
    
    this.SetQuizId = function(quiz_id)
    {   
        for(var i=0; i<this.grades.length; i++)
        {
            this.grades[i].quiz_id = quiz_id;
        }
        return true;
    };
};