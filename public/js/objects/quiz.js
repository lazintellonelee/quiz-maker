var Quiz = function()
{
    this.quiz = {
        quiz_id         : 0,
        quiz_force      : 0,
        quiz_title      : '',
        quiz_category   : 0,
        time_out        : null,
        one_time        : false,
        questions       : [
            {
                quiz_id : 0,
                content : '',
                display : 1,
                type    : 'select_one',
                index   : 0,
                answers : [
                    {
                        question_id : 0,
                        index       : 0,
                        content     : '',
                        score       : 0
                    },
                    {
                        question_id : 0,
                        index       : 1,
                        content     : '',
                        score       : 0
                    },
                    {
                        question_id : 0,
                        index       : 2,
                        content     : '',
                        score       : 0
                    }
                ]
            }
        ]
    };
    
    this.GetData = function() 
    {
        return this.quiz;
    };
    
    this.GetMaxScore = function()
    {
        var questions   = this.quiz.questions;
        var max_score   = 0;
        for(var i=0; i<questions.length; i++)
        {
            var type        = questions[i].type;
            var tmp_score   = 0;
            for(var j=0; j<questions[i].answers.length; j++)
            {
                if(type === 'select_one')
                {
                    if(questions[i].answers[j].score !== 0 && questions[i].answers[j].score > tmp_score)
                    {
                        tmp_score = questions[i].answers[j].score;
                    }
                }
                else
                {
                    if(questions[i].answers[j].score !== 0)
                    {
                        tmp_score = tmp_score + questions[i].answers[j].score;
                    }
                }
            }
            max_score = max_score + tmp_score;
        }
        return max_score;
    };
    
    this.GetQuiz = function()
    {
        return {
            quiz_title  : this.quiz.quiz_title
        };
    };
    
    this.SetQuizCategory = function(quiz_category)
    {
        this.quiz.quiz_category = quiz_category;
    };
    
    this.SetQuizTimeOut = function(time_out)
    {
        this.quiz.time_out = time_out;
    };
    
    this.SetQuizForce = function(force)
    {
        this.quiz.quiz_force = force;
    };
    
    this.SetQuizOneTime = function(one_time)
    {
        this.quiz.one_time = Boolean(one_time);
    };
    
    this.SetQuizTitle = function(title)
    {
        this.quiz.quiz_title = title;
        return true;
    };
    
    this.GetQuestions = function()
    {
        return this.quiz.questions;
    };
    
    this.GetQuestionByIndex = function(index)
    {
        for(var i=0; i<this.quiz.questions.length; i++)
        {
            if(this.quiz.questions[i].index === index)
            {
                return this.quiz.questions[i];
            }
        }
        return false;
    };
    
    this.AddQuestion = function()
    {
        var len = this.quiz.questions.length;
        this.quiz.questions[len] = {
            content : '',
            display : 1,
            type    : 'select_one',
            index   : len,
            answers : [
                {
                    index   : 0,
                    content : '',
                    score   : 0
                },
                {
                    index   : 1,
                    content : '',
                    score   : 0
                },
                {
                    index   : 2,
                    content : '',
                    score   : 0
                }
            ]
        };
        return {
            content : '',
            display : 1,
            type    : 'select_one',
            index   : len,
            answers : [
                {
                    index   : 0,
                    content : '',
                    score   : 0
                },
                {
                    index   : 1,
                    content : '',
                    score   : 0
                },
                {
                    index   : 2,
                    content : '',
                    score   : 0
                }
            ]
        };
    };
    
    this.SetQuestionType = function(index, type)
    {
        for(var i=0; i<this.quiz.questions.length; i++)
        {
            if(this.quiz.questions[i].index === index)
            {
                this.quiz.questions[i].type = type;
                return true;
            }
        }
        return true;
    };
    
    this.SetQuestionDisplay = function(index, display)
    {
        for(var i=0; i<this.quiz.questions.length; i++)
        {
            if(this.quiz.questions[i].index === index)
            {
                this.quiz.questions[i].display = display;
                return true;
            }
        }
        return true;
    };
    
    this.SetQuestionContent = function(index, content)
    {
        for(var i=0; i<this.quiz.questions.length; i++)
        {
            if(this.quiz.questions[i].index === index)
            {
                this.quiz.questions[i].content = content;
                return true;
            }
        }
        return true;
    };
    
    this.RemoveQuestionByIndex = function(index)
    {
        for(var i=0; i<this.quiz.questions.length; i++)
        {
            if(this.quiz.questions[i].index === index)
            {
                this.quiz.questions.splice(i, 1);
                return true;
            }
        }
        return true;
    };
    
    this.RemoveAnswerByIndex = function(question_index, answer_index)
    {
        for(var i=0; i<this.quiz.questions.length; i++)
        {
            if(this.quiz.questions[i].index === question_index)
            {
                for(var j=0; j<this.quiz.questions[i].answers.length; j++)
                {
                    if(this.quiz.questions[i].answers[j].index === answer_index)
                    {
                        this.quiz.questions[i].answers.splice(j, 1);
                        return true;
                    }
                }
            }
        }
        return true;
    };
    
    this.GetAnswer = function(question_index, answer_index)
    {
        for(var i=0; i<this.quiz.questions.length; i++)
        {
            if(this.quiz.questions[i].index === question_index)
            {
                for(var j=0; j<this.quiz.questions[i].answers.length; j++)
                {
                    if(this.quiz.questions[i].answers[j].index === answer_index)
                    {
                        return {
                            index   : this.quiz.questions[i].answers[j].index,
                            content : this.quiz.questions[i].answers[j].content,
                            score   : this.quiz.questions[i].answers[j].score
                        };
                    }
                }
            }
        }
        return true;
    };
    
    this.AddAnswer = function(question_index)
    {
        for(var i=0; i<this.quiz.questions.length; i++)
        {
            if(this.quiz.questions[i].index === question_index)
            {
                var len     = this.quiz.questions[i].answers.length;
                this.quiz.questions[i].answers[len] = {
                    index   : len,
                    content : '',
                    score   : 0
                };;
                return {
                    index   : len,
                    content : '',
                    score   : 0
                };
            }
        }
        return true;
    };
    
    this.SetAnswerContent = function(question_index, answer_index, content)
    {
        for(var i=0; i<this.quiz.questions.length; i++)
        {
            if(this.quiz.questions[i].index === question_index)
            {
                for(var j=0; j<this.quiz.questions[i].answers.length; j++)
                {
                    if(this.quiz.questions[i].answers[j].index === answer_index)
                    {
                        this.quiz.questions[i].answers[j].content = content;
                        return true;
                    }
                }
            }
        }
        return true;
    };
    
    this.SetAnswerScore = function(question_index, answer_index, score)
    {
        for(var i=0; i<this.quiz.questions.length; i++)
        {
            if(this.quiz.questions[i].index === question_index)
            {
                for(var j=0; j<this.quiz.questions[i].answers.length; j++)
                {
                    if(this.quiz.questions[i].answers[j].index === answer_index)
                    {
                        this.quiz.questions[i].answers[j].score = score;
                        return true;
                    }
                }
            }
        }
        return true;
    };
};