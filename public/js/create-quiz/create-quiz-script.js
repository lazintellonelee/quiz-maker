var quiz    = new Quiz();
var grades  = new Grades();
var params  = parseQuerySrc('public/js/create-quiz/create-quiz-script.js');
$(document).ready(function()
{
    $('#quiz-create-tab a[data-toggle="tab"]').on('shown.bs.tab', {
        quiz : quiz
    }, function (event) {
        var quiz    = event.data.quiz;
        var target  = $(event.target).attr("href");
        if(target !== '#quiz')
        {
            $('.container-question').children('.add-answer-container').remove();
            $('.container-question').children('.question-setting').remove();
        }
        
        if(target === '#results')
        {
            $('#score').children('.result-container-question').remove();
            $('#score').html(tmpl('tmpl-result-container', quiz.GetData()));
        }
    });
    
    $(document).on('click', '.create-quiz', {
        quiz    : quiz,
        grades  : grades,
        params  : params
    }, function(event){
        event.preventDefault();
        var quiz    = event.data.quiz;
        var grades  = event.data.grades;
        var params  = event.data.params;
        
        $('#quiz-create-tab').addClass('hidden').before(tmpl('tmpl-quiz-creating', {
            'hasGrades' : grades.GetPostGrades().length
        }));
        $('#quiz-create-pane').addClass('hidden');
        
        $.ajax({
            method  : 'POST',
            url     : params.url + '/create-quiz.html',
            cache   : false,
            data    : quiz.GetData(),
            headers : {
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            }
        }).complete(function(jqXHR){
            console.log(jqXHR.responseText);
            var result = JSON.parse(jqXHR.responseText);
            if(result.error !== undefined)
            {
                return true;
            }
            
            $('.creating-quiz').addClass('text-success').html('<i class="fa fa-fw fa-check"></i> Create quiz success.');
            grades.SetQuizId(result.quiz_id);
            
            if(grades.GetPostGrades().length === 0)
            {
                $('.container-process').after(tmpl('tmpl-share-container', result));
                return true;
            }
            
            $.ajax({
                method  : 'POST',
                url     : params.url + '/create-grades.html',
                cache   : false,
                data    : {
                    grades : grades.GetPostGrades()
                },
                headers : {
                    'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                }
            }).complete(function(jqXHR){
                console.log(jqXHR.responseText);
                var result = JSON.parse(jqXHR.responseText);
                if(result.error !== undefined)
                {
                    return true;
                }
                $('.creating-grade').addClass('text-success').html('<i class="fa fa-fw fa-check"></i> Create grades success.');
                
                $('.container-process').after(tmpl('tmpl-share-container', result));
                return true;
            });
            return true;
        });
    });
});