$(document).ready(function(){
    $(document).on('click', '.quiz-category', {
        quiz : quiz
    }, function(event){
        var quiz            = event.data.quiz;
        var quiz_category   = $(this).attr('quiz-category');
        quiz.SetQuizCategory(quiz_category);
        
        if(quiz_category === 'survey_poll')
        {
            $('a[href=#results]').parents('li[role=presentation]').addClass('hidden');
            $('a[href=#setting]').parents('li[role=presentation]').addClass('hidden');
            $('a.add-question').addClass('hidden');
        }
        else {
            $('a[href=#results]').parents('li[role=presentation]').removeClass('hidden');
            $('a[href=#setting]').parents('li[role=presentation]').removeClass('hidden');
            $('a.add-question').removeClass('hidden');
        }
        
        $('.quiz-category').not(this).removeClass('active');
        $(this).addClass('active');
        
        var txt = "";
        txt     = txt + '<i class="fa fa-fw ' + $(this).children('i').attr('class').split(' ')[2] + '"></i> ';
        txt     = txt + $(this).children('h4').html();
        
        $('.container-quiz-category-content').slideUp();
        $('.hidden-quiz-category').addClass('hidden');
        $('.quiz-category-value').removeClass('hidden').html(txt);
        return true;
    });
    
    $(document).on('click', '.hidden-quiz-category', function(event){
        event.preventDefault();
        $('.container-quiz-category-content').slideUp();
        $('.quiz-category-value').removeClass('hidden');
        $(this).addClass('hidden');
        return true;
    });
    
    $(document).on('click', '.quiz-category-value', function(event){
        event.preventDefault();
        $('.container-quiz-category-content').slideDown();
        $('.hidden-quiz-category').removeClass('hidden');
        $(this).addClass('hidden');
        return true;
    });
});
