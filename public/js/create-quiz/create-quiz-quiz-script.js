$(document).ready(function(){
    $('#quiz').children('div:last()').before(tmpl('tmpl-quiz-container', quiz.GetQuiz()));
    
    quiz.GetQuestions().forEach(function(element, index) {
        $('#quiz').children('div:last()').before(tmpl('tmpl-question-container', element));
    }); 
    
    $(document).on('click', '.container-question', {
        quiz : quiz
    }, function(event){
        var quiz = event.data.quiz;
        
        $('.container-quiz').removeClass('active');
        $('.container-question').not(this).removeClass('active');
        if($('.container-question').not(this).children('.add-answer-container').length > 0 && $('.container-question').not(this).children('.question-setting').length > 0)
        {
            $('.container-question').not(this).children('.add-answer-container').remove();
            $('.container-question').not(this).children('.question-setting').remove();
        }
        $('.container-question').not(this).children('.add-answer-container').remove();
        $('.container-question').not(this).children('.question-setting').remove();
        if($(this).children('.add-answer-container').length === 0 && $(this).children('.question-setting').length === 0)
        {
            var question_index  = parseInt($(this).attr('question-index'));
            var question        = quiz.GetQuestionByIndex(question_index);
            $(this).addClass('active').append(tmpl('tmpl-question-setting', question));
            $(this).find('select.question-display').val(question.display);
        }
    });
    
    $(document).on('click', '.container-quiz', function(event){
        $('.container-question').removeClass('active');
        if($('.container-question').children('.add-answer-container').length > 0 && $('.container-question').children('.question-setting').length > 0)
        {
            $('.container-question').children('.add-answer-container').remove();
            $('.container-question').children('.question-setting').remove();
        }
        $(this).addClass('active');
    });
    
    $(document).on('click', '.add-question', {
        quiz : quiz
    }, function(event){
        event.preventDefault();
        var quiz        = event.data.quiz;
        var question    = quiz.AddQuestion();
        $('#quiz').children('div:last()').before(tmpl('tmpl-question-container', question));
    });
    
    $(document).on('click', '.close-question', {
        quiz : quiz
    }, function(event){
        event.preventDefault();
        var quiz    = event.data.quiz;
        var index   = parseInt($(this).parents('.container-question').attr('question-index'));
        quiz.RemoveQuestionByIndex(index);
        $(this).parents('.container-question').remove();
        return true;
    });
    
    $(document).on('click', '.close-answer', {
        quiz : quiz
    }, function(event){
        event.preventDefault();
        var quiz            = event.data.quiz;
        var index           = parseInt($(this).parents('.answer').attr('answer-index'));
        var question_index  = parseInt($(this).parents('.container-question').attr('question-index'));
        quiz.RemoveAnswerByIndex(question_index, index);
        $(this).parents('.answer').remove();
        return true;
    });
    
    $(document).on('click', '.add-answer', {
        quiz : quiz
    }, function(event){
        event.preventDefault();
        var quiz            = event.data.quiz;
        var question_index  = parseInt($(this).parents('.container-question').attr('question-index'));
        var answer          = quiz.AddAnswer(question_index);
        var question        = quiz.GetQuestionByIndex(question_index);
        answer.col          = 12/question.display;
        if($(this).parents('.container-question').children('.answer').length > 0)
        {
            $(this).parents('.container-question').children('.answer:last()').after(tmpl('tmpl-answer', answer));
            return true;
        }
        $(this).parents('.container-question').children('.question').after(tmpl('tmpl-answer', answer));
        $(this).parents('.container-question').find('input').attr('placeholder','Type You First Answer ...');
        return true;
    });
    
    $(document).on('click', '.btn-question-type', {
        quiz : quiz
    }, function(event){
        var quiz            = event.data.quiz;
        var question_index  = parseInt($(this).parents('.container-question').attr('question-index'));
        var type            = $(this).attr('data-type');
        var question        = quiz.GetQuestionByIndex(question_index);
        if(type === question.type)
        {
            return true;
        }
        quiz.SetQuestionType(question_index, type);
        $(this).addClass('active');
        $('.btn-question-type').not(this).removeClass('active');
        return true;
    });
    
    $(document).on('change', '.question-display', {
        quiz : quiz
    }, function(event){
        var quiz            = event.data.quiz;
        var question_index  = parseInt($(this).parents('.container-question').attr('question-index'));
        quiz.SetQuestionDisplay(question_index, $(this).val());
        
        var answers         = $(this).parents('.container-question').children('.answer');
        for(var i=0; i<answers.length; i++)
        {
            var str_class   = $(answers[i]).attr('class');
            var classes     = str_class.split(" ");
            classes[0]      = 'col-sm-' + 12/$(this).val();
            $(answers[i]).attr('class', classes.join(" "));
        }
    });
    
    
    
    $(document).on('change', 'input#quiz-title', {
        quiz : quiz
    }, function(event){
        var quiz            = event.data.quiz;
        var title           = $(this).val();
        quiz.SetQuizTitle(title);
    });
    
    $(document).on('change', '.question textarea', {
        quiz : quiz
    }, function(event){
        var quiz            = event.data.quiz;
        var question_index  = parseInt($(this).parents('.container-question').attr('question-index'));
        var content         = $(this).val();
        quiz.SetQuestionContent(question_index, content);
    });
    
    $(document).on('change', '.answer input', {
        quiz : quiz
    }, function(event){
        var quiz            = event.data.quiz;
        var answer_index    = parseInt($(this).parents('.answer').attr('answer-index'));
        var question_index  = parseInt($(this).parents('.container-question').attr('question-index'));
        var content         = $(this).val();
        quiz.SetAnswerContent(question_index, answer_index, content);
    });
});