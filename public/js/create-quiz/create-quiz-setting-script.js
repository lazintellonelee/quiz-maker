$(document).ready(function(){
    $('#slide').slider({
        orientation : "horizontal",
        range       : "min",
        min         : 1,
        max         : 90,
        value       : 60,
        slide       : function (event, ui) {
            $("#time-out-value").html(ui.value);
            quiz.SetQuizTimeOut(parseInt(ui.value));
            return true;
        }
    });
    
    $(document).on('change', '#check-use-time-out', {
        quiz : quiz
    }, function(event){
        var quiz    = event.data.quiz;
        var flag    = $(this).prop('checked');
        if(flag) 
        {
            $('.set-time-out-container').fadeIn();
            quiz.SetQuizTimeOut(parseInt($("#slide").slider("value")));
            return true;
        }
        $('.set-time-out-container').fadeOut();
        quiz.SetQuizTimeOut(null);
        return true;
    });
    
    $(document).on('change', '#check-only-one-time', {
        quiz : quiz
    }, function(event){
        var flag    = Boolean($(this).prop('checked'));
        var quiz    = event.data.quiz;
        quiz.SetQuizOneTime(flag);
        return true;
    });
    
    $(document).on('change', '#check-use-force', {
        quiz : quiz
    }, function(event){
        var flag    = Boolean($(this).prop('checked'))===true?1:0;
        var quiz    = event.data.quiz;
        quiz.SetQuizForce(flag);
        return true;
    });
});