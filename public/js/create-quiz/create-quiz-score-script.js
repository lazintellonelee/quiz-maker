$(document).ready(function(){
    $(document).on('click', '.container-result', {
        quiz : quiz
    }, function(event){
        event.preventDefault();
        var quiz            = event.data.quiz;
        var question_index  = parseInt($(this).parents('.result-container-question').attr('question-index'));
        var answer_index    = parseInt($(this).attr('answer-index'));
        
        $(this).toggleClass('active');
        $(this).find('.answer-status').html('<i class="fa fa-fw fa-times"></i>');
        if($(this).hasClass('active'))
        {
            $(this).find('.answer-status').html('<i class="fa fa-fw fa-check"></i>');
            $(this).children('.result-score').find('input').val(1);
            quiz.SetAnswerScore(question_index, answer_index, 1);
            return true;
        }
        quiz.SetAnswerScore(question_index, answer_index, 0);
        return true;
    });
    
    $(document).on('click', '.result-score input', {
        quiz : quiz
    }, function(event){
        return false;
    }).parents('.container-result').click(function(event){
        return false;
    });;
    
    $(document).on('keyup', '.result-score input', {
        quiz : quiz
    }, function(event){
        var quiz            = event.data.quiz;
        var question_index  = parseInt($(this).parents('.result-container-question').attr('question-index'));
        var answer_index    = parseInt($(this).parents('.container-result').attr('answer-index'));
        var score           = parseInt($(this).val());
        if(score === 0)
        {
            $(this).parents('.container-result').removeClass('active');
            $(this).parents('.container-result').find('.answer-status').html('<i class="fa fa-fw fa-times"></i>');
            quiz.SetAnswerScore(question_index, answer_index, score);
            return true;
        }
        quiz.SetAnswerScore(question_index, answer_index, score);
        return true;
    });
    
    $(document).on('change', '.result-score input', {
        quiz : quiz
    }, function(event){
        var quiz            = event.data.quiz;
        var question_index  = parseInt($(this).parents('.result-container-question').attr('question-index'));
        var answer_index    = parseInt($(this).parents('.container-result').attr('answer-index'));
        var score           = parseInt($(this).val()===''?0:$(this).val());
        if(score === 0)
        {
            $(this).parents('.container-result').removeClass('active');
            $(this).parents('.container-result').find('.answer-status').html('<i class="fa fa-fw fa-times"></i>');
            quiz.SetAnswerScore(question_index, answer_index, score);
            return true;
        }
        quiz.SetAnswerScore(question_index, answer_index, score);
        return true;
    });
});