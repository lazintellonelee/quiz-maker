$(document).ready(function(){
    $('#score-tab a[data-toggle="tab"]').on('shown.bs.tab', {
        quiz    : quiz,
        grades  : grades
    }, function (event) {
        var quiz    = event.data.quiz;
        var grades  = event.data.grades;
        var target  = $(event.target).attr("href");
        $('#results').find('a.add-grade').remove();
        if(target === '#grades')
        {
            var _grades = grades.GetGrades();
            $('#grades').children('.container-grade').remove();
            for(var i=0; i<_grades.length; i++)
            {
                $('#grades').append(tmpl('tmpl-grade-container', _grades[i]));
            }
            $('#results').children('div:last()').children('div').append('<a class="btn btn-default btn-no-radius add-grade"><i class="fa fa-fw fa-plus"></i> Add grade</a>');
            
        }
    });
    
    $(document).on('click', '.add-grade', {
        grades : grades
    }, function(event) {
        event.preventDefault();
        var grades = event.data.grades;
        $('#grades').append(tmpl('tmpl-grade-container', grades.AddGrade()));
        return true;
    });
    
    $(document).on('change', '.grade-title input', {
        grades : grades
    }, function(event){
        event.preventDefault();
        var grades      = event.data.grades;
        var grade_index = parseInt($(this).parents('.container-grade').attr('grade-index'));
        var title       = $(this).val();
        grades.SetGradeTitle(grade_index, title);
        return true;
    });
    
    $(document).on('change', '.grade-description textarea', {
        grades : grades
    }, function(event){
        event.preventDefault();
        var grades      = event.data.grades;
        var grade_index = parseInt($(this).parents('.container-grade').attr('grade-index'));
        var description = $(this).val();
        grades.SetGradeDescription(grade_index, description);
        return true;
    });
    
    $(document).on('change', 'select.grade-type', {
        grades : grades
    }, function(event){
        event.preventDefault();
        var grades      = event.data.grades;
        var grade_index = parseInt($(this).parents('.container-grade').attr('grade-index'));
        var type        = $(this).val();
        grades.SetGradeType(grade_index, type);
        return true;
    });
    
    $(document).on('change', 'input.grade-from', {
        grades : grades
    }, function(event){
        event.preventDefault();
        var grades      = event.data.grades;
        var grade_index = parseInt($(this).parents('.container-grade').attr('grade-index'));
        var from        = parseInt($(this).val());
        grades.SetGradeFrom(grade_index, from);
        return true;
    });
    
    $(document).on('change', 'input.grade-to', {
        grades : grades
    }, function(event){
        event.preventDefault();
        var grades      = event.data.grades;
        var grade_index = parseInt($(this).parents('.container-grade').attr('grade-index'));
        var to          = parseInt($(this).val());
        grades.SetGradeTo(grade_index, to);
        return true;
    });
    
    $(document).on('click', '.close-grade', {
        grades : grades
    }, function(event){
        event.preventDefault();
        var grades      = event.data.grades;
        var grade_index = parseInt($(this).parents('.container-grade').attr('grade-index'));
        $(this).parents('.container-grade').remove();
        grades.RemoveGrade(grade_index);
        return true;
    });
});