var parseQuery = function (queryString) {var query = queryString.replace(/^[^\?]+\??/, ''); var Params = new Object(); if (!query) return Params; var Pairs = query.split(/[;&]/); for (var i = 0; i < Pairs.length; i++) {var KeyVal = Pairs[i].split('='); if (!KeyVal || KeyVal.length !== 2) continue; var key = unescape(KeyVal[0]); var val = unescape(KeyVal[1]); val = val.replace(/\+/g, ' '); Params[key] = val; } return Params; }; var parseQuerySrc = function (src_string) {var scripts = document.getElementsByTagName('script'); var length = scripts.length; var src = ''; for (var i = 0; i < length; i++) {if (scripts[i].hasAttribute('src')) {if (scripts[i].getAttribute('src').indexOf(src_string) !== -1) {src = scripts[i].getAttribute('src'); break; } } } var params = parseQuery(src); return params; }; var getElementScript = function (src_string) {var scripts = document.getElementsByTagName('script'); var length = scripts.length; var src = ''; for (var i = 0; i < length; i++) {if (scripts[i].hasAttribute('src')) {if (scripts[i].getAttribute('src').indexOf(src_string) !== -1) {return scripts[i]; } } } };
/* Function to build css style */
var BuildCss=function(selector, style){var string=selector+"{"; for(var item in style){string=string+item+':'+style[item]+';'} string=string+"}";return string;};

var Quiz = function()
{
    /* Main Container */
    this.container          = getElementScript('embed/quiz-maker-embed.js').parentNode;
    /* Script Container */
    this.scriptContainer    = getElementScript('embed/quiz-maker-embed.js');
    /* Content contain question and answers*/
    this.content            = null;
    /* Height */
    this.height     = 0;
    /* Information Quiz */
    this.quiz       = null;
    /* Information Question */
    this.questions  = null;
    /* Information Params */
    this.params     = parseQuerySrc('embed/quiz-maker-embed.js'); 
    /* URL base website quiz-maker */
    this.url        = 'http://localhost/quiz-maker';
    this.GetQuiz(function(quiz){
        quiz.SetHeightContent();
        quiz.BuildStyle();
        quiz.BuildInterface();
    });
};
window.addEventListener('load',function(){var quiz=new Quiz();},false);
/* Function for get quiz information and questions information */
Quiz.prototype.GetQuiz = function(callback)
{
    var _this   = this;
    var http    = new XMLHttpRequest();
    var form    = new FormData();
    form.append('quiz_id', this.params.q);
    form.append('quiz_secret_key', this.params.sk);
    http.open('POST', this.url + '/embed-get-quiz.html', true);
    http.onload = function(event)
    {
        var result      = JSON.parse(this.responseText);
        console.log(result);
        _this.quiz      = result.quiz;
        _this.questions = result.questions;
        callback(_this);
    };
    http.send(form);
};
/* Check use bootstrap interface */
Quiz.prototype.CheckBootstrap = function()
{
    if(this.params.bootstrap !== undefined)
    {
        return true;
    }
    return false;
};
/* Build interface */
Quiz.prototype.BuildInterface = function()
{
    if(this.quiz === null || this.questions === null){return false;}
    this.BuildTitle().BuildContainerContentQuiz().BuildReadyQuiz();
};
/* Build title quiz interface */
Quiz.prototype.BuildTitle = function()
{
    if(this.quiz === null){return false;}
    var div = document.createElement('div');
    div.setAttribute(
        'class', 'quiz-container-title' 
        + (this.CheckBootstrap()?' col-sm-12 col-xs-12':'')
    );
    div.appendChild(document.createTextNode(this.quiz.quiz_title));
    this.container.insertBefore(div, this.scriptContainer);
    return this;
};
/* Build content container quiz */
Quiz.prototype.BuildContainerContentQuiz = function()
{
    var content = document.createElement('div');
    content.setAttribute(
        'class', 'quiz-container-content' 
        + (this.CheckBootstrap()?' col-sm-12 col-xs-12':'')
    );
    this.content = content;
    this.container.insertBefore(this.content, this.scriptContainer);
    return this;
};
/* Clear content quiz */
Quiz.prototype.ClearContent = function()
{
    this.content.innerHTML = '';
    return this;
};
/* Build ready to do quiz */
Quiz.prototype.BuildReadyQuiz = function()
{
    var quiz    = this;
    var button  = document.createElement('button');
    button.setAttribute('class', 'btn btn-info btn-large quiz-button-ready');
    button.appendChild(document.createTextNode('Bắt đầu'));
    button.addEventListener('click', function(){
        quiz.ClearContent();
        quiz.BuildContent();
    }, false);
    this.content.appendChild(button);
    return this;
};
/* Get Question */
Quiz.prototype.GetQuestion = function(index)
{
    if(index === undefined || index < 0){index=0;}
    if(index > this.questions.length - 1){index=this.questions.length-1;}
    return this.questions[index];
};
/* Build Content Quiz  */
Quiz.prototype.BuildContent = function(index)
{
    this.BuildQuestionContent().BuildAnswersContent();
};
/* Build Content Quiz All Question */
Quiz.prototype.BuildContentAllQuestions = function()
{
    console.log('Build content all questions');
};
/* Build Question Content */
Quiz.prototype.BuildQuestionContent = function(index)
{
    if(index === undefined || index < 0){index=0;}
    var question    = this.GetQuestion(index);
    var div         = document.createElement('div');
    div.setAttribute(
        'class', 'quiz-container-question-content' 
        + (this.CheckBootstrap()?' col-sm-12 col-xs-12':'')
    );
    var bold = document.createElement('b');
    bold.appendChild(document.createTextNode('Câu ' + (index+1) + ': '));
    
    div.appendChild(bold);
    div.appendChild(document.createTextNode(question.question_content));
    this.content.appendChild(div);
    return this;
};
/* Handle Checked Answer */
Quiz.prototype.HandleCheckedAnswer = function(inputDOM)
{
    var question_index  = parseInt(inputDOM.getAttribute('question-index'));
    var answer_index    = parseInt(inputDOM.getAttribute('answer-index'));
    this.questions[question_index].answers.forEach(function(answer, index){
        answer.checked  = false;
    });
    this.questions[question_index].answers[answer_index].checked = true;
    if(parseInt(this.quiz.quiz_force) !== 1){return this;};
    
    var next= question_index + 1;
    if(next > this.questions.length - 1){
        this.BuildWaittingResult().GetResult();
        return this;
    }
    this.ClearContent().BuildQuestionContent(next).BuildAnswersContent(next);
    return this;
};
/* Build Quiz Answer */
Quiz.prototype.BuildAnswersContent = function(index)
{
    if(index === undefined || index < 0){index=0;}
    var _this       = this;
    var question    = this.questions[index];
    var answers     = question.answers;
    
    var container   = document.createElement('div');
    container.setAttribute(
        'class', 'quiz-container-answers'
        + (this.CheckBootstrap()?' col-sm-12 col-xs-12':'')
    );
    
    for(var i in answers)
    {
        var id      = 'answer_' + index + '_' + i;
        var content = document.createElement('div');
        content.setAttribute(
            'class', 'quiz-container-answer'
            + (this.CheckBootstrap()?' col-sm-' + (12/question.question_display) + ' col-xs-' + (12/question.question_display):'')
        );

        var input       = document.createElement('input');
        input.checked   = answers[i].checked;
        input.setAttribute('id', id);
        input.setAttribute('name', 'question_' + index);
        input.setAttribute('type', question.question_type==='select_one'?'radio':'checkbox');
        input.setAttribute('class', 'quiz-container-answer-input');
        input.setAttribute('question-index', index);
        input.setAttribute('answer-index', i);
        input.addEventListener('change', function(){
            _this.HandleCheckedAnswer(this);
        }, false);
        
        var label   = document.createElement('label');
        label.setAttribute('for', id);
        label.setAttribute('class', 'quiz-container-answer-label');
        label.appendChild(document.createTextNode(answers[i].answer_content));
        
        content.appendChild(input);
        content.appendChild(label);
        
        container.appendChild(content);
    }
    this.content.appendChild(container);
    return this;
};
/* Set Height Content */
Quiz.prototype.SetHeightContent = function()
{
    var max_height = 0;
    for(var i=0; i<this.questions.length; i++)
    {
        var t_max_height    = 0;
        var question_height = (parseInt((this.questions[i].question_content.length*3)/this.container.offsetWidth)===0?1:parseInt((this.questions[i].question_content.length*3)/this.container.offsetWidth)) * 30;
        
        var row_answer      = parseInt(this.questions[i].answers.length / this.questions[i].question_display);
        var answer_width    = parseInt(this.container.offsetWidth / this.questions[i].question_display);
        var answer_height   = 0;
        for(var j=0; j<this.questions[i].answers.length; j++)
        {
            var t_height = (parseInt((this.questions[i].answers[j].answer_content.length*3)/answer_width)===0?1:parseInt((this.questions[i].answers[j].answer_content.length*3)/answer_width))*35;
            if(t_height > answer_height)
            {
                answer_height = t_height;
            }
        }
        t_max_height = answer_height*row_answer + question_height;
        if(t_max_height > max_height)
        {
            max_height = t_max_height;
        }
    }
    max_height  = max_height + 30;
    this.height = max_height;
    if(this.quiz.quiz_type === 'do_all')
    {
        this.height = this.height * this.questions.length;
    }
    return this.height;
};
/* Get ID Questions */
Quiz.prototype.GetQuestionId = function()
{
    var questions = [];
    for(var i=0; i<this.questions.length; i++)
    {
        questions[questions.length] = this.questions[i].question_id;
    }
    return questions;
};
/* Get Answer Checked */
Quiz.prototype.GetAnswersChecked = function()
{
    var answers = [];
    for(var i=0; i<this.questions.length; i++)
    {
        for(var j=0; j<this.questions[i].answers.length; j++)
        {
            if(this.questions[i].answers[j].checked === true)
            {
                answers[answers.length] = this.questions[i].answers[j].answer_id;
            }
        }
    }
    return answers;
};
/* Get Result */
Quiz.prototype.GetResult = function()
{
    var _this   = this;
    var form    = new FormData();
    var http    = new XMLHttpRequest();
    
    form.append('answers', this.GetAnswersChecked());
    form.append('questions', this.GetQuestionId());
    form.append('quiz_id', this.quiz.quiz_id);
    
    http.open('POST', this.url + '/embed-get-result.html', true);
    http.onload = function(event)
    {
        var result  = JSON.parse(this.responseText);
        var div     = document.createElement('div');
        div.setAttribute(
            'class', 'quiz-container-result'
            + (_this.CheckBootstrap()?' col-sm-12 col-xs-12':'')
        );
        var text    = document.createTextNode(result.score + '/' + result.max_score);
        div.appendChild(text);
        _this.ClearContent();
        _this.content.appendChild(div);
        return true;
    };
    http.send(form);
    return true;
};
/* Build interface waitting result */
Quiz.prototype.BuildWaittingResult = function()
{
    var img = document.createElement('img');
    img.setAttribute('class', 'img-responsive quiz-image-waitting-result');
    img.setAttribute('src', 'http://tracuu.thuvientphcm.gov.vn:8081/Images/loader.gif');
    
    this.ClearContent();
    this.content.appendChild(img);
    return this;
};

/* Build style for quiz */
Quiz.prototype.BuildStyle = function()
{
    var style = [
        BuildCss('.quiz-container-title', {
            'background-color':'#5bc0de',
            'padding':'15px',
            'text-align':'center',
            'color':'#fff',
            'font-size':'20px',
            'min-height':'60px'
        }),
        BuildCss('.quiz-container-content', {
            'min-height':this.height + 'px',
            'border':'1px solid #5bc0de',
            'position':'relative'
        }),
        BuildCss('.quiz-button-ready', {
            'height':'50px',
            'width':'80px',
            'margin':'-50px -40px',
            'border-radius':'0px',
            'position':'absolute',
            'top':'60%',
            'left':'50%'
        }),
        BuildCss('.quiz-container-question-content', {
            'padding':'10px 15px'
        }),
        BuildCss('input.quiz-container-answer-input', {
            'display':'none',
        }),
        BuildCss('label.quiz-container-answer-label', {
            'font-weight':'normal',
            'margin-left':'10px'
        }),
        BuildCss('label.quiz-container-answer-label:before',{
            'content': "''",
            'display': 'inline-block',
            'width': '16px',
            'height': '16px',
            'background-color':'#5bc0de',
            'position':'absolute',
            'left':'0px',
            'top':'1px'
        }),
        BuildCss('input[type=radio].quiz-container-answer-input + label.quiz-container-answer-label:before', {
            'border-radius':'8px'
        }),
        BuildCss('input[type=radio].quiz-container-answer-input:checked + label.quiz-container-answer-label:before', {
            'content': "'\\2022'",
            'color': '#fff',
            'font-size': '29px',
            'text-align': 'center',
            'line-height': '17px'
        }),
        BuildCss('input[type=checkbox].quiz-container-answer-input:checked + label.quiz-container-answer-label:before', {
            'content': "'\\2713'",
            'color': '#fff',
            'font-size': '25px',
            'text-align': 'center',
            'line-height': '17px'
        }),
        BuildCss('.quiz-container-answer',{
            'margin' : '5px auto',
        }),
        BuildCss('.quiz-image-waitting-result', {
            'height':'50px',
            'width':'50px',
            'margin':'-50px -25px',
            'border-radius':'0px',
            'position':'absolute',
            'top':'60%',
            'left':'50%'
        }),
        BuildCss('.quiz-container-result', {
            'font-size':'30px',
            'color':'#5bc0de',
            'margin-top':this.height/2 - 30 + 'px',
            'text-align':'center'
        })
    ];
    var tag = document.createElement('style');
    tag.appendChild(document.createTextNode(style.join(style, '')));
    
    document.getElementsByTagName('head')[0].appendChild(tag);
};
