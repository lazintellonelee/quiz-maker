var parseQuery = function (queryString) {var query = queryString.replace(/^[^\?]+\??/, ''); var Params = new Object(); if (!query) return Params; var Pairs = query.split(/[;&]/); for (var i = 0; i < Pairs.length; i++) {var KeyVal = Pairs[i].split('='); if (!KeyVal || KeyVal.length !== 2) continue; var key = unescape(KeyVal[0]); var val = unescape(KeyVal[1]); val = val.replace(/\+/g, ' '); Params[key] = val; } return Params; }; var parseQuerySrc = function (src_string) {var scripts = document.getElementsByTagName('script'); var length = scripts.length; var src = ''; for (var i = 0; i < length; i++) {if (scripts[i].hasAttribute('src')) {if (scripts[i].getAttribute('src').indexOf(src_string) !== -1) {src = scripts[i].getAttribute('src'); break; } } } var params = parseQuery(src); return params; }; var getElementScript = function (src_string) {var scripts = document.getElementsByTagName('script'); var length = scripts.length; var src = ''; for (var i = 0; i < length; i++) {if (scripts[i].hasAttribute('src')) {if (scripts[i].getAttribute('src').indexOf(src_string) !== -1) {return scripts[i]; } } } };

var q_quiz              = null;
var q_questions         = null;
var q_baseUrlQuizMaker  = 'http://localhost/quiz-maker';
var q_params            = parseQuerySrc('embed/quiz-maker.quiz-embed.js');
var q_script            = getElementScript('embed/quiz-maker.quiz-embed.js');
var q_container         = q_script.parentNode;
var q_height            = 0;
q_container.setAttribute('style', 'border:1px solid #e5e5e5;padding:0px');

/*
 * Hàm dùng để lấy kết quả quiz
 */
var GetResult = function() {
    var form = new FormData();
    var http = new XMLHttpRequest();
    
    form.append('answers', GetAnswerHasChecked());
    form.append('questions', GetQuestionsID());
    form.append('quiz_id', q_quiz.quiz_id);
    
    http.open('POST', q_baseUrlQuizMaker + '/embed-get-result.html', true);
    http.onload = function(event)
    {
        var result  = JSON.parse(this.responseText);
        q_container.removeChild(document.getElementsByClassName('quiz-container-main')[0]);
        q_container.removeChild(document.getElementsByClassName('quiz-container-pagination')[0]);
       
        var div     = document.createElement('div');
        div.setAttribute('class', 'col-sm-12 col-xs-12 quiz-container-result');
        div.appendChild(document.createTextNode(result.score + '/' + result.max_score));
        
        q_container.appendChild(div);
        return true;
    };
    http.send(form);
    return true;
};

/*
 * Hàm dùng để lấy nhưng câu trả lời được chọn
 */

var GetAnswerHasChecked = function()
{
    var answers = [];
    for(var i=0; i<q_questions.length; i++)
    {
        for(var j=0; j<q_questions[i].answers.length; j++)
        {
            if(q_questions[i].answers[j].checked === true)
            {
                answers[answers.length] = q_questions[i].answers[j].answer_id;
            }
        }
    }
    return answers;
};

/*
 * Hàm dùng để lấy question id
 */
var GetQuestionsID = function()
{
    var questions = [];
    for(var i=0; i<q_questions.length; i++)
    {
        questions[questions.length] = q_questions[i].question_id;
    }
    return questions;
};

/*
 * Hàm dùng để lấy height cao nhất
 */
var SetMaxHeight = function()
{
    var max_height = 0;
    for(var i=0; i<q_questions.length; i++)
    {
        var t_max_height    = 0;
        var question_height = (parseInt((q_questions[i].question_content.length*3)/q_container.offsetWidth)===0?1:parseInt((q_questions[i].question_content.length*3)/q_container.offsetWidth)) * 30;
        
        var row_answer      = parseInt(q_questions[i].answers.length / q_questions[i].question_display);
        var answer_width    = parseInt(q_container.offsetWidth / q_questions[i].question_display);
        var answer_height   = 0;
        for(var j=0; j<q_questions[i].answers.length; j++)
        {
            var t_height = (parseInt((q_questions[i].answers[j].answer_content.length*3)/answer_width)===0?1:parseInt((q_questions[i].answers[j].answer_content.length*3)/answer_width))*35;
            if(t_height > answer_height)
            {
                answer_height = t_height;
            }
        }
        t_max_height = answer_height*row_answer + question_height;
        if(t_max_height > max_height)
        {
            max_height = t_max_height;
        }
    }
    q_height =  max_height + 55;
};

/*
 * Cập nhật trạng thái câu trả lời chọn/không
 */
var ChangeCheckedAnswer = function(question_id, answer_id, check)
{
    for(var i=0; i<q_questions.length; i++)
    {
        if(q_questions[i].question_id === question_id)
        {
            for(var j=0; j<q_questions[i].answers.length; j++)
            {
                if(q_questions[i].answers[j].answer_id === answer_id)
                {
                    q_questions[i].answers[j].checked = check;
                    continue;
                }
                
                if(q_questions[i].question_type === 'select_one')
                {
                    q_questions[i].answers[j].checked = false;
                }
            }
            return true;
        }
    }
    return true;
}

/*
 * Hàm này để xây dụng style ở head
 */
var SetStyleHeader = function()
{
    var theme   = '#5bc0de';
    var style   = document.createElement('style');
    var text    = document.createTextNode('.quiz-container-main {height: ' + q_height + 'px; } .quiz-container-title {background-color:#5bc0de; padding-top:5px; padding-bottom:5px; color: #fff; } .quiz-container-start {height: ' + (q_height - 19) + 'px; } .quiz-button-start {margin-top:' + (q_height - 65)/2 + 'px; } .quiz-container-question {padding-top:5px; padding-bottom:5px; padding-left:30px; } .quiz-container-answer {padding-top:5px; padding-bottom:5px; padding-left:30px; } label.quiz_label_check {display: inline-block; cursor: pointer; position: relative; padding-left: 25px; margin-right: 15px; font-size: 13px; } input[type=radio].quiz_input_check {display: none; } label.quiz_label_check:before {content: ""; display: inline-block; width: 16px; height: 16px; margin-right: 10px; position: absolute; left: 0; bottom: 1px; background-color: #5bc0de; } input[type=radio].quiz_input_check + label.quiz_label_check:before {border-radius: 8px; } input[type=radio].quiz_input_check:checked + label.quiz_label_check:before {content: "\\2022"; color: #fff; font-size: 29px; text-align: center; line-height: 17px; } input[type=checkbox].quiz_input_check {display: none; } label.quiz_label_check:before {content: ""; display: inline-block; width: 16px; height: 16px; margin-right: 10px; position: absolute; left: 0; bottom: 1px; background-color: #5bc0de; } input[type=checkbox].quiz_input_check + label.quiz_label_check:before {border-radius: 3px; } input[type=checkbox].quiz_input_check:checked + label.quiz_label_check:before {content: "\\2713"; color: #fff; font-size: 18px; text-align: center; line-height: 17px; } .quiz-container-pagination {margin-top: 0px; text-align: center; padding-bottom: 10px; } a.quiz-container-pagination-next {margin-left: 15px; font-weight: bold; font-size: 30px; text-decoration: none; line-height: 15px; } a.quiz-container-pagination-previous {margin-right: 15px; font-weight: bold; font-size: 30px; text-decoration: none; line-height: 15px; } .quiz-container-result {padding:' + (q_height - 65)/2 + 'px; font-size: 30px; text-align: center; font-weight: 600; color: #5bc0de; }');
    style.appendChild(text);
    
    document.getElementsByTagName('head')[0].appendChild(style);
};

/*
 * Cái này dùng để xây dựng giao diện khi không tồn tại quiz
 */
var GetHMTLEmptyQuiz = function()
{
    var div = document.createElement('div');
    div.setAttribute('class', 'col-sm-12 col-xs-12');

    var img = document.createElement('img');
    img.setAttribute('class', 'img-responsive img-rounded');
    img.setAttribute('src', 'http://www.springfieldproject.org.uk/upload/image/quiz_150908823.jpg');

    div.appendChild(img);
    q_container.insertBefore(div, q_script);
    return true;
};

/*
 * Hàm này để lấy question theo pagination
 */
var GetPaginatonQuestion = function(index)
{
    if(index >= q_questions.length)
    {
        index = 0;
    }
    
    if(index < 0)
    {
        index = q_questions.length - 1;
    }
    q_container.insertBefore(GetHTMLQuestion(index), document.getElementsByClassName('quiz-container-pagination')[0]);
    document.getElementsByClassName('quiz-container-pagination-index')[0].innerHTML = index + 1;
    document.getElementsByClassName('quiz-container-pagination-next')[0].setAttribute('question-index' , index + 1);
    document.getElementsByClassName('quiz-container-pagination-previous')[0].setAttribute('question-index' ,index - 1);;
};

/*
 * Hàm này để xây dựng giao diện pagination
 */
var GetHTMLPagination = function(index)
{
    var div = document.createElement('div');
    div.setAttribute('class', 'col-sm-12 col-xs-12 text-right quiz-container-pagination');
    if(q_quiz.quiz_force === 1)
    {
        div.appendChild(document.createTextNode('\240'));
        return div;
    }
    
    if(index === undefined)
    {
        index = 0;
    }
    var question_index = document.createElement('span');
    question_index.setAttribute('class', 'quiz-container-pagination-index');
    question_index.appendChild(document.createTextNode(index + 1));
    
    var next = document.createElement('a');
    next.setAttribute('class', 'pull-right quiz-container-pagination-next');
    next.setAttribute('question-index', index+1);
    next.setAttribute('href', '#');
    next.appendChild(document.createTextNode('\u2192'));
    next.addEventListener('click', function(event){
        event.preventDefault();
        q_container.removeChild(document.getElementsByClassName('quiz-container-question')[0].parentNode);
        GetPaginatonQuestion(parseInt(document.getElementsByClassName('quiz-container-pagination-next')[0].getAttribute('question-index')))
    }, false);
    
    var previous = document.createElement('a');
    previous.setAttribute('class', 'pull-left quiz-container-pagination-previous');
    previous.setAttribute('question-index', index-1);
    previous.setAttribute('href', '#');
    previous.appendChild(document.createTextNode('\u2190'));
    previous.addEventListener('click', function(event){
        event.preventDefault();
        q_container.removeChild(document.getElementsByClassName('quiz-container-question')[0].parentNode);
        GetPaginatonQuestion(parseInt(document.getElementsByClassName('quiz-container-pagination-previous')[0].getAttribute('question-index')))
    }, false);
    
    div.appendChild(previous);
    div.appendChild(question_index);
    div.appendChild(next);
    
    /*
    div.appendChild(document.createTextNode('\240'));
    */
    return div;
};

/*
 * Cái này dùng để xây dựng giao diện tiêu để trắc nghiệm
 */
var GetHTMLTitle = function(text)
{
    var title   = document.createElement('div');
    title.setAttribute('class', 'col-sm-12 col-xs-12 text-center quiz-container-title');
    
    var head_4  = document.createElement('h4');
    head_4.appendChild(document.createTextNode(text));
    title.appendChild(head_4);
    
    return title;
};

/*
 * Cái này dùng để xây giao diện button start trắc nghiệm
 */
var GetHTMLContainerStart = function()
{
    var start_container = document.createElement('div');
    start_container.setAttribute('class', 'col-sm-12 col-xs-12 text-center quiz-container-start');
    
    var button = document.createElement('button');
    button.setAttribute('class', 'btn btn-info btn-lg quiz-button-start');
    button.appendChild(document.createTextNode('Bắt đầu'));
    
    start_container.appendChild(button);
    
    
    button.addEventListener('click', function(){
        q_container.removeChild(document.getElementsByClassName('quiz-container-start')[0]);
        q_container.insertBefore(GetHTMLQuestion(), q_script);
        if(q_questions.length > 1)
        {
            q_container.insertBefore(GetHTMLPagination(), q_script);
        }
    }, true);
    return start_container;
};

/*
 * Cái này dùng để xây dựng câu hỏi và câu trả lời
 */
var GetHTMLQuestion = function(index)
{
    var row = document.createElement('div');
    row.setAttribute('class', 'row quiz-container-main');
    row.setAttribute('style', 'padding-top:15px;padding-bottom:15px;');
    
    var question            = GetQuestion(index);
    var question_container  = document.createElement('div');
    question_container.setAttribute('class', 'col-sm-12 col-xs-12 quiz-container-question');
    
    var question_index      = document.createElement('b');
    question_index.appendChild(document.createTextNode('Câu ' + (index!==undefined?index+1:1) + '.'));
    question_container.appendChild(question_index);
    
    var question_content    = document.createTextNode(' ' + question.question_content);
    question_container.appendChild(question_content);
    row.appendChild(question_container);
    
    for(var i=0; i<question.answers.length; i++)
    {
        var answer              = question.answers[i];
        var id                  = "answer_" + answer.answer_id;
        var answer_container    = document.createElement('div');
        answer_container.setAttribute('class', 'col-sm-' + (12/question.question_display) + ' col-xs-' + (12/question.question_display) + ' quiz-container-answer');
        
        var checkbox = document.createElement('input');
        
        if(question.question_type === "select_one")
        {
            checkbox.setAttribute('type', 'radio');
        }
        else{
            checkbox.setAttribute('type', 'checkbox');
        }
        
        if(question.question_type === "select_one")
        {
            checkbox.setAttribute('name', 'answer');
        }
        
        if(answer.checked === true)
        {
            checkbox.setAttribute('checked', true);
        }
        checkbox.setAttribute('id', id);
        checkbox.setAttribute('class', 'quiz_input_check');
        checkbox.setAttribute('question-id', question.question_id);
        checkbox.setAttribute('answer-id', answer.answer_id);
        
        checkbox.addEventListener('change',function(event){
            var check       = this.checked;
            var question_id = parseInt(this.getAttribute('question-id'));
            var answer_id   = parseInt(this.getAttribute('answer-id'));
            ChangeCheckedAnswer(question_id, answer_id, check);
            
            if(q_quiz.quiz_force === 1)
            {
                if(index+1 >= q_questions.length)
                {
                    GetResult();
                    return true;
                }
                q_container.removeChild(document.getElementsByClassName('quiz-container-question')[0].parentNode);
                q_container.insertBefore(GetHTMLQuestion(index===undefined?1:(index+1)), document.getElementsByClassName('quiz-container-pagination')[0]);
                
            }
        }, false);
        
        answer_container.appendChild(checkbox);
        
        var answer_content      = document.createElement('label');
        answer_content.setAttribute('for', id);
        answer_content.setAttribute('class', 'quiz_label_check');
        answer_content.appendChild(document.createTextNode(answer.answer_content));
        
        answer_container.appendChild(answer_content);
        row.appendChild(answer_container);
    }
    return row;
};

var GetQuestion = function(index)
{
    if(index === undefined)
    {
        index = 0;
    }
    
    if(index >= q_questions.length)
    {
        index = 0;
    }
    
    if(index < 0)
    {
        index = q_questions.length - 1;
    }
    
    return q_questions[index];
}

/*
var text                = document.createTextNode('Water');
q_container.insertBefore(text, q_script);
*/
var q_http  = new XMLHttpRequest();
var q_form  = new FormData();
q_form.append('quiz_id', q_params.q);
q_form.append('quiz_secret_key', q_params.sk);
q_http.open('POST', q_baseUrlQuizMaker + '/embed-get-quiz.html', true);
q_http.onload = function(event)
{
    var result  = JSON.parse(this.responseText);
    q_quiz      = result.quiz;
    q_questions = result.questions;
    
    SetMaxHeight();
    SetStyleHeader();
    
    if(result.no_quiz !== undefined)
    {
        GetHMTLEmptyQuiz();
        return retrue;
    }
    
    q_container.insertBefore(GetHTMLTitle(q_quiz.quiz_title), q_script);
    q_container.insertBefore(GetHTMLContainerStart(), q_script);
};
q_http.send(q_form);