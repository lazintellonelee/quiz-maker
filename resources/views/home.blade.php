@extends('layout.default')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ url('public/css/home.css') }}" />
@endsection

@section('content')
<div class="container">
<div class="col-sm-12 col-xs-12 margin-20">
    <input type="text" class="form-control input-no-radius" placeholder="Search Quiz ..." />
</div>

<div class="col-sm-12 col-xs-12 margin-20">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">New</a></li>
        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Hot</a></li>
        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Vote</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <div class="col-sm-12 col-xs-12 quiz-container">
                <h3><a href="#">Trắc ngiệm vật lý lớp 12</a></h3>
                <div class="col-sm-4 col-xs-12 no-padding">
                    <b>Author: </b>
                    <a href="#">Lazintellone Lee</a>
                </div>
                <div class="col-sm-8 col-xs-12 no-padding">
                    <a href="#">
                        <i class="fa fa-fw fa-thumbs-o-up"></i> 125
                    </a>
                    
                    <a href="#">
                        <i class="fa fa-fw fa-pencil-square-o"></i> 125
                    </a>
                </div>
            </div>
            
            <div class="col-sm-12 col-xs-12 quiz-container">
                <h3><a href="#">Trắc ngiệm vật Hoá Học 12</a></h3>
                <div class="col-sm-4 col-xs-12 no-padding">
                    <b>Author: </b>
                    <a href="#">Lazintellone Lee</a>
                </div>
                <div class="col-sm-8 col-xs-12 no-padding">
                    <a href="#">
                        <i class="fa fa-fw fa-thumbs-o-up"></i> 125
                    </a>
                    
                    <a href="#">
                        <i class="fa fa-fw fa-pencil-square-o"></i> 125
                    </a>
                </div>
            </div>
            
            <div class="col-sm-12 col-xs-12 quiz-container">
                <h3><a href="#">Trắc ngiệm vật lý lớp 12</a></h3>
                <div class="col-sm-4 col-xs-12 no-padding">
                    <b>Author: </b>
                    <a href="#">Lazintellone Lee</a>
                </div>
                <div class="col-sm-8 col-xs-12 no-padding">
                    <a href="#">
                        <i class="fa fa-fw fa-thumbs-o-up"></i> 125
                    </a>
                    
                    <a href="#">
                        <i class="fa fa-fw fa-pencil-square-o"></i> 125
                    </a>
                </div>
            </div>
            
            <div class="col-sm-12 col-xs-12 quiz-container">
                <h3><a href="#">Trắc ngiệm vật Hoá Học 12</a></h3>
                <div class="col-sm-4 col-xs-12 no-padding">
                    <b>Author: </b>
                    <a href="#">Lazintellone Lee</a>
                </div>
                <div class="col-sm-8 col-xs-12 no-padding">
                    <a href="#">
                        <i class="fa fa-fw fa-thumbs-o-up"></i> 125
                    </a>
                    
                    <a href="#">
                        <i class="fa fa-fw fa-pencil-square-o"></i> 125
                    </a>
                </div>
            </div>
            
            <div class="col-sm-12 col-xs-12 quiz-container">
                <h3><a href="#">Trắc ngiệm vật lý lớp 12</a></h3>
                <div class="col-sm-4 col-xs-12 no-padding">
                    <b>Author: </b>
                    <a href="#">Lazintellone Lee</a>
                </div>
                <div class="col-sm-8 col-xs-12 no-padding">
                    <a href="#">
                        <i class="fa fa-fw fa-thumbs-o-up"></i> 125
                    </a>
                    
                    <a href="#">
                        <i class="fa fa-fw fa-pencil-square-o"></i> 125
                    </a>
                </div>
            </div>
            
            <div class="col-sm-12 col-xs-12 quiz-container">
                <h3><a href="#">Trắc ngiệm vật Hoá Học 12</a></h3>
                <div class="col-sm-4 col-xs-12 no-padding">
                    <b>Author: </b>
                    <a href="#">Lazintellone Lee</a>
                </div>
                <div class="col-sm-8 col-xs-12 no-padding">
                    <a href="#">
                        <i class="fa fa-fw fa-thumbs-o-up"></i> 125
                    </a>
                    
                    <a href="#">
                        <i class="fa fa-fw fa-pencil-square-o"></i> 125
                    </a>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="profile">
            <div class="col-sm-12 col-xs-12 quiz-container">
                <h3><a href="#">Trắc ngiệm vật lý lớp 12</a></h3>
                <div class="col-sm-4 col-xs-12 no-padding">
                    <b>Author: </b>
                    <a href="#">Lazintellone Lee</a>
                </div>
                <div class="col-sm-8 col-xs-12 no-padding">
                    <a href="#">
                        <i class="fa fa-fw fa-thumbs-o-up"></i> 125
                    </a>
                    
                    <a href="#">
                        <i class="fa fa-fw fa-pencil-square-o"></i> 125
                    </a>
                </div>
            </div>
            
            <div class="col-sm-12 col-xs-12 quiz-container">
                <h3><a href="#">Trắc ngiệm vật Hoá Học 12</a></h3>
                <div class="col-sm-4 col-xs-12 no-padding">
                    <b>Author: </b>
                    <a href="#">Lazintellone Lee</a>
                </div>
                <div class="col-sm-8 col-xs-12 no-padding">
                    <a href="#">
                        <i class="fa fa-fw fa-thumbs-o-up"></i> 125
                    </a>
                    
                    <a href="#">
                        <i class="fa fa-fw fa-pencil-square-o"></i> 125
                    </a>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="messages">
            <div class="col-sm-12 col-xs-12 quiz-container">
                <h3><a href="#">Trắc ngiệm vật lý lớp 12</a></h3>
                <div class="col-sm-4 col-xs-12 no-padding">
                    <b>Author: </b>
                    <a href="#">Lazintellone Lee</a>
                </div>
                <div class="col-sm-8 col-xs-12 no-padding">
                    <a href="#">
                        <i class="fa fa-fw fa-thumbs-o-up"></i> 125
                    </a>
                    
                    <a href="#">
                        <i class="fa fa-fw fa-pencil-square-o"></i> 125
                    </a>
                </div>
            </div>
            
            <div class="col-sm-12 col-xs-12 quiz-container">
                <h3><a href="#">Trắc ngiệm vật Hoá Học 12</a></h3>
                <div class="col-sm-4 col-xs-12 no-padding">
                    <b>Author: </b>
                    <a href="#">Lazintellone Lee</a>
                </div>
                <div class="col-sm-8 col-xs-12 no-padding">
                    <a href="#">
                        <i class="fa fa-fw fa-thumbs-o-up"></i> 125
                    </a>
                    
                    <a href="#">
                        <i class="fa fa-fw fa-pencil-square-o"></i> 125
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12 col-xs-12 margin-20 publish-container">
    <h2 class="text-center">
        How to Publish a Quiz in 3 Steps
    </h2>
    
    <div class="col-sm-4 col-xs-12 margin-5 text-center publish-container-content">
        <i class="fa fa-fw fa-sign-in fa-5x"></i>
        <h3>1. Sign Up</h3>
        <div class="col-sm-12 col-xs-12 margin-5">
            On the Themes tab select one of our default themes or create your own. On the settings tab set options like allowing multiple answers, allowing voters to enter their own answers and much more
        </div>
    </div>
    
    <div class="col-sm-4 col-xs-12 margin-5 text-center publish-container-content">
        <i class="fa fa-fw fa-pencil-square-o fa-5x"></i>
        <h3>2. Make Quiz</h3>
        <div class="col-sm-12 col-xs-12 margin-5">
            On the Themes tab select one of our default themes or create your own. On the settings tab set options like allowing multiple answers, allowing voters to enter their own answers and much more
        </div>
    </div>
    
    <div class="col-sm-4 col-xs-12 margin-5 text-center publish-container-content">
        <i class="fa fa-fw fa-share-alt fa-5x"></i>
        <h3>3. Share</h3>
        <div class="col-sm-12 col-xs-12 margin-5">
            On the Themes tab select one of our default themes or create your own. On the settings tab set options like allowing multiple answers, allowing voters to enter their own answers and much more
        </div>
    </div>
</div>
</div>
@endsection