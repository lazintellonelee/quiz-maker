<div class="modal fade" id="modal-signup">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body row">
                <form method="post" id="form-sign-up" data-login="{{ route('log-in')}}" data-signup="{{ route('sign-up') }}">
                    <div class="col-sm-12 col-xs-12 text-right">
                        <a id="change-action-signup" data-action="signup" style="font-size: 12px" href="#">Sign up</a>
                    </div>

                    <div class="col-sm-12 col-xs-12 hidden" id="error-content">
                        <div class="alert alert-warning alert-dismissible" role="alert"></div>
                    </div>

                    <div class="col-sm-12 col-xs-12 margin-5">
                        <input type="text" id="signup-email" name="user_email" class="form-control input-no-radius" placeholder="Email" autocomplete="off" />
                    </div>

                    <div class="col-sm-12 col-xs-12 margin-5" style="display: none">
                        <input type="text" id="signup-retype-email" class="form-control input-no-radius" placeholder="Retype Email" autocomplete="off" />
                    </div>

                    <div class="col-sm-12 col-xs-12 margin-5" style="display: none">
                        <input type="text" id="signup-username" name="user_name" class="form-control input-no-radius" placeholder="Username" autocomplete="off" />
                    </div>

                    <div class="col-sm-12 col-xs-12 margin-5">
                        <input type="password" id="signup-password" name="user_password" class="form-control input-no-radius" placeholder="Password" autocomplete="off" />
                    </div>

                    <div class="col-sm-12 col-xs-12 margin-5">
                        <button id="signup-button" type="button" class="btn btn-success btn-no-radius btn-full">Log in</button>
                    </div>

                    <div class="col-sm-12 col-xs-12 margin-5">
                        <h2 class="line">
                            <span>OR</span>
                        </h2>
                    </div>

                    <div class="col-sm-12 col-xs-12 margin-5">
                        <a class="btn btn-primary btn-no-radius btn-full">
                            <i class="fa fa-fw fa-facebook"></i> Login with Facebook
                        </a>
                    </div>

                    <div class="col-sm-12 col-xs-12 margin-5">
                        <a class="btn btn-danger btn-no-radius btn-full">
                            <i class="fa fa-fw fa-google"></i> Login with Google
                        </a>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->