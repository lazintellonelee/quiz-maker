<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('home') }}">Quiz Maker</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="<?= !get_quiz_user()?'':'hidden'?>">
                    <a id="sign-up" href="#">Sign In</a>
                </li>
                
                <li class="dropdown <?= !get_quiz_user()?'hidden':''?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span id="nav-user-name">
                            {{ get_quiz_user_name() }}
                        </span> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Account</a></li>
                        <li><a href="{{ route('create-quiz') }}">Create Quiz</a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="{{ route('sign-out') }}">Sign out</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>