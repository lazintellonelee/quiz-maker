@extends('layout.default')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/jquery-ui/css/custom-theme/jquery-ui-1.10.3.custom.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/checkbox/normalize.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/plugins/checkbox/style.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ url('public/css/create-quiz.css') }}" />
@endsection

@section('js')
<script type="text/javascript" src="{{ url('public/plugins/jquery-ui/assets/js/vendor/jquery-ui-1.10.3.custom.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/plugins/javascript-template/tmpl.min.js') }}"></script>
<script type="text/javascript" src="{{ url('public/js/functions.js') }}"></script>
<script type="text/javascript" src="{{ url('public/js/objects/quiz.js') }}"></script>
<script type="text/javascript" src="{{ url('public/js/objects/grades.js') }}"></script>
<script type="text/javascript" src="{{ url('public/js/create-quiz/create-quiz-script.js?url='.url()) }}"></script>
<script type="text/javascript" src="{{ url('public/js/create-quiz/create-quiz-quiz-script.js') }}"></script>
<script type="text/javascript" src="{{ url('public/js/create-quiz/create-quiz-category-script.js') }}"></script>
<script type="text/javascript" src="{{ url('public/js/create-quiz/create-quiz-score-script.js') }}"></script>
<script type="text/javascript" src="{{ url('public/js/create-quiz/create-quiz-setting-script.js') }}"></script>
<script type="text/javascript" src="{{ url('public/js/create-quiz/create-quiz-grades-script.js') }}"></script>
@endsection

@section('content')
<div class="col-sm-offset-2 col-sm-8 col-xs-12 margin-20">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist" id="quiz-create-tab">
        <li role="presentation" class="active"><a href="#quiz" aria-controls="home" role="tab" data-toggle="tab">Quiz</a></li>
        <li role="presentation"><a href="#results" aria-controls="profile" role="tab" data-toggle="tab">Scoring</a></li>
        <li role="presentation"><a href="#setting" aria-controls="messages" role="tab" data-toggle="tab">Setting</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content" id="quiz-create-pane">
        <div role="tabpanel" class="tab-pane active" id="quiz">
            <div class="col-sm-12 col-xs-12 margin-10 no-padding container-quiz-category">
                <div class="col-sm-12 col-xs-12 no-padding container-quiz-category-content">
                    <div class="col-sm-6 col-xs-12">
                        <div class="col-sm-12 col-xs-12 margin-5 quiz-category text-center" quiz-category="quiz_question">
                            <i class="fa fa-fw fa-question fa-5x"></i>
                            <h4>Quiz / Question</h4>
                        </div>
                    </div>

                    <div class="col-sm-6 col-xs-12">
                        <div class="col-sm-12 col-sm-12 margin-5 quiz-category text-center" quiz-category="survey_poll">
                            <i class="fa fa-fw fa-bar-chart fa-5x"></i>
                            <h4>Survey / Poll</h4>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-xs-12 no-padding container-quiz-category-footer text-center">
                    <a href="#" class="hidden-quiz-category">
                        <i class="fa fa-fw fa-caret-up"></i>
                    </a>

                    <a href="#" class="quiz-category-value hidden">
                        Quiz Category
                    </a>
                </div>
            </div>

            <div class="col-sm-12 col-xs-12 no-padding">
                <div class="col-sm-12 col-xs-12 no-padding">
                    <a class="btn btn-default btn-no-radius add-question"><i class="fa fa-fw fa-plus"></i> Add Question</a>
                    <a class="btn btn-success btn-no-radius pull-right create-quiz">Create Quiz</a>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="results">
            <div class="clearfix margin-10"></div>

            <ul class="nav nav-tabs" role="tablist" id="score-tab">
                <li style="width:50%;text-align:center" role="presentation" class="active"><a href="#score" aria-controls="home" role="tab" data-toggle="tab">Scored</a></li>
                <li style="width:50%;text-align:center" role="presentation"><a href="#grades" aria-controls="profile" role="tab" data-toggle="tab">Grades</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="score"></div>
                <div role="tabpanel" class="tab-pane" id="grades">
                    <div class="col-sm-12 col-xs-12 no-padding margin-10">
                        <i class="fa fa-fw fa-info"></i> <i>Add your list of grades and set the minimum and maximum scores to make them show</i>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-xs-12 margin-10 no-padding">
                <div class="col-sm-12 col-xs-12 no-padding">
                    <a class="btn btn-success btn-no-radius pull-right create-quiz">Create Quiz</a>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="setting">
            <div class="col-sm-12 col-xs-12 margin-10 container-setting">
                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-12 no-padding"><i>Use force</i></div>
                    <div class="col-sm-10 col-xs-12 no-padding">
                        <input type="checkbox" id="check-use-force" name="check-use-force" />
                        <label for="check-use-force">&nbsp;</label>
                    </div>
                </div>

                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-12 no-padding"><i>Only one time</i></div>
                    <div class="col-sm-10 col-xs-12 no-padding">
                        <input type="checkbox" id="check-only-one-time" name="check-only-one-time" />
                        <label for="check-only-one-time">&nbsp;</label>
                    </div>
                </div>

                <div class="col-sm-12 col-xs-12 no-padding margin-5">
                    <div class="col-sm-2 col-xs-12 no-padding"><i>Use time out</i></div>
                    <div class="col-sm-10 col-xs-12 no-padding">
                        <input type="checkbox" id="check-use-time-out" name="check-use-time-out" />
                        <label for="check-use-time-out">&nbsp;</label>
                    </div>
                </div>

                <div class="col-sm-12 col-xs-12 no-padding margin-5 set-time-out-container">
                    <div class="col-sm-2 col-xs-12 no-padding">
                        <i>Time out - </i>
                        <i><span id="time-out-value">60</span>'</i>
                    </div>
                    <div class="col-sm-10 col-xs-12 no-padding margin-5" id="slide"></div>
                </div>
            </div>

            <div class="col-sm-12 col-xs-12 margin-10 container-setting">
                <div class="col-sm-12 col-xs-12 no-padding">
                    <h4>Quiz should be unlocked</h4>
                    <i class="fa fa-fw fa-info"></i> Add quiz must be completed before do this quiz.
                </div>

                <div class="col-sm-12 col-xs-12 no-padding text-right">
                    <a href="#" class="add-quiz-before"><i class="fa fa-fw fa-plus"></i> Add Quiz</a>
                </div>
            </div>

            <div class="col-sm-12 col-xs-12 margin-10 no-padding">
                <div class="col-sm-12 col-xs-12 no-padding">
                    <a class="btn btn-success btn-no-radius pull-right create-quiz">Create Quiz</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/t-tmpl" id="tmpl-quiz-container">
    <div class="col-sm-12 col-xs-12 margin-10 container-quiz">
        <input id="quiz-title" type="text" class="form-control input-no-radius" placeholder="Quiz Title ..." value="{%= o.quiz_title%}" />
    </div>
</script>

<script type="text/t-tmpl" id="tmpl-answer">
    <div class="col-sm-{%= o.col%} col-xs-12 no-padding-right padding-left-30 margin-5 answer" answer-index="{%= o.index%}">
        <input type="text" class="form-control input-no-radius" value="{%= o.content%}" />
        <a href="#" class="close-answer"><i class="fa fa-fw fa-times"></i></a>
        <a href="#" class="attach-image"><i class="fa fa-fw fa-image"></i></a>
    </div>
</script>

<script type="text/t-tmpl" id="tmpl-question-container"> 
    <div class="col-sm-12 col-xs-12 margin-10 container-question" question-index="{%= o.index%}">
        <div class="col-sm-12 col-xs-12 question-index no-padding text-right">
            Question {%= o.index + 1%}
        </div>

        <div class="col-sm-12 col-xs-12 no-padding margin-5 question">
            <textarea class="form-control input-no-radius" placeholder="Type Your Question ...">{%= o.content%}</textarea>
            <a href="#" class="close-question"><i class="fa fa-fw fa-times"></i></a>
        </div>
        {% for(var i=0; i<o.answers.length; i++) { %}
        <div class="col-sm-{%= 12/o.display%} col-xs-12 no-padding-right padding-left-30 margin-5 answer" answer-index="{%= o.answers[i].index%}">
            <input type="text" class="form-control input-no-radius" placeholder="{%= i==0?'Type Your First Answer ...':''%}" value="{%= o.answers[i].content%}" />
            <a href="#" class="close-answer"><i class="fa fa-fw fa-times"></i></a>
            <a href="#" class="attach-image"><i class="fa fa-fw fa-image"></i></a>
        </div>
        {% } %}
    </div>
</script>

<script type="text/t-tmpl" id="tmpl-question-setting">
    <div class="col-sm-12 col-xs-12 no-padding-right padding-left-30 margin-5 add-answer-container">
        <a href="#" class="add-answer">Add answer</a>
    </div>

    <div class="col-sm-12 col-xs-12 no-padding margin-10 question-setting">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#type" aria-controls="home" role="tab" data-toggle="tab">Type</a></li>
            <li role="presentation"><a href="#display" aria-controls="profile" role="tab" data-toggle="tab">Display</a></li>
        </ul>

    <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="type">
                <div class="col-sm-6 col-xs-6 padding-top-bottom-10">
                    <a data-type="select_one" class="btn-question-type btn btn-default btn-no-radius btn-full {%= o.type==='select_one'?'active':''%}">
                        <i class="fa fa-fw fa-check-circle-o"></i> Select One
                    </a>
                </div>
                <div class="col-sm-6 col-xs-6 padding-top-bottom-10">
                    <a data-type="multi_choice" class="btn-question-type btn btn-default btn-no-radius btn-full {%= o.type==='multi_choice'?'active':''%}">
                        <i class="fa fa-fw fa-check-square-o"></i> Multi Choise
                    </a>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="display">
                <div class="col-sm-12 col-xs-12 padding-top-bottom-10">
                    <div class="col-sm-1 col-xs-12 div-line">
                        Answer
                    </div>
                    <div class="col-sm-5 col-xs-12">
                        <select class="form-control input-no-radius question-display">
                            <option value="1">1 Column</option>
                            <option value="2">2 Columns</option>
                            <option value="3">3 Columns</option>
                            <option value="4">4 Columns</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>

<script type="text/x-tmpl" id="tmpl-result-container"> 
    {% for(var i=0; i<o.questions.length; i++) { %}
    <div class="col-sm-12 col-xs-12 margin-10 result-container-question" question-index="{%= o.questions[i].index%}">
        <div class="col-sm-12 col-xs-12 question-index no-padding">
            <b>Question {%= o.questions[i].index + 1%}</b>{%= o.questions[i].content===''?'':'. '+o.questions[i].content%}
        </div>
        {% for(var j=0; j<o.questions[i].answers.length; j++) { %}
        <div class="col-sm-12 col-xs-12 no-padding margin-5 container-result {%= o.questions[i].answers[j].score==0?'':'active'%}" answer-index="{%= o.questions[i].answers[j].index%}">
            <div class="col-sm-11 col-xs-10 div-line">
                <span class="answer-status">
                    <i class="fa fa-fw fa-{%= o.questions[i].answers[j].score==0?'times':'check'%}"></i>
                </span> 
                {%= o.questions[i].answers[j].content===''?'Empty':o.questions[i].answers[j].content%}
            </div>
            <div class="col-sm-1 col-xs-2 no-padding result-score">
                <input type="number" placeholder="0" min="0" class="form-control input-no-radius" value="{%= o.questions[i].answers[j].score==0?'':o.questions[i].answers[j].score%}" />
            </div>
        </div>
        {% } %}
    </div>
    {% } %}
</script>

<script type="text/x-tmpl" id="tmpl-grade-container">
    <div class="col-sm-12 col-xs-12 margin-10 container-grade" grade-index="{%= o.index%}">
        <div class="col-sm-12 col-xs-12 margin-5 no-padding grade-title">
            <input class="form-control input-no-radius" placeholder="Grade Title ..." value="{%= o.grade_title%}" />
            <a href="" class="close-grade"><i class="fa fa-fw fa-times"></i></a>
            <a href="" class="attach-image"><i class="fa fa-fw fa-image"></i></a>
        </div>

        <div class="col-sm-12 col-xs-12 margin-5 no-padding grade-description">
            <textarea class="form-control input-no-radius" rows="4" style="resize:none" placeholder="Grade Description ...">{%= o.grade_description%}</textarea>
        </div>

        <div class="col-sm-12 col-xs-12 margin-10 no-padding grade-setting">
            <div class="col-sm-1 col-xs-2 div-line">Type</div>
            <div class="col-sm-2 col-xs-2">
                <select class="form-control input-no-radius grade-type">
                    <option {%= o.grade_type==='score'?'selected':''%} value="score">Score</option>
                    <option {%= o.grade_type==='percent'?'selected':''%} value="percent">Percent</option>
                </select>
            </div> 
            <div class="col-sm-1 col-xs-2 div-line">From</div>
            <div class="col-sm-2 col-xs-2">
                <input type="number" min="0" class="form-control input-no-radius grade-from" placeholder="From ..." value="{%= o.grade_from%}" />
            </div>
            <div class="col-sm-1 col-xs-2 div-line">To</div>
            <div class="col-sm-2 col-xs-2">
                <input type="number" min="0" class="form-control input-no-radius grade-to" placeholder="To ..." value="{%= o.grade_to%}" />
            </div>
        </div>
    </div>
</script>

<script type="text/x-tmpl" id="tmpl-quiz-creating">
    <div class="col-sm-12 col-xs-12 container-process margin-10">
        <div class="col-sm-12 col-xs-12 no-padding  creating-quiz">
            <i class="fa fa-spinner fa-pulse"></i> Creating quiz ...
        </div>
        {% if(o.hasGrades > 0) { %}
        <div class="col-sm-12 col-xs-12 no-padding  creating-grade">
            <i class="fa fa-spinner fa-pulse"></i> Creating grades ...
        </div>
        {% } %}
    </div>
</script>

<script type="text/x-tmpl" id="tmpl-share-container">
    <div class="col-sm-12 col-xs-12 container-share">
        <ul class="nav nav-tabs" role="tablist" id="container-result-tab">
            <li role="presentation" class="active"><a href="#share" aria-controls="home" role="tab" data-toggle="tab">Share</a></li>
            <li role="presentation"><a href="#embed" aria-controls="profile" role="tab" data-toggle="tab">Embed</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content" id="container-result-pane">
            <div role="tabpanel" class="tab-pane active" id="share">
                <div class="col-sm-12 col-xs-12 no-padding container-result-share">
                    <div class="col-sm-12 col-xs-12 margin-10 no-padding">
                        <a class="btn btn-primary btn-no-radius"><i class="fa fa-fw fa-facebook"></i></a>
                        <a class="btn btn-danger btn-no-radius"><i class="fa fa-fw fa-google-plus"></i></a>
                    </div>

                    <div class="col-sm-12 col-xs-12 margin-10 no-padding">
                        <div class="col-sm-1 col-xs-1 no-padding div-line">
                            <b><i class="fa fa-fw fa-link"></i> Link</b>
                        </div>
                        <div class="col-sm-11 col-xs-11 no-padding div-line">
                            <i>http://quiz-maker.com/quiz/612aj1kaj1ks-7</i>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="embed">
                <div class="col-sm-12 col-xs-12 no-padding container-result-embed">
                    <div class="col-sm-12 col-xs-12 no-padding margin-5">
                        Embed below this code in your website:
                    </div>

                    <div class="col-sm-12 col-xs-12 container-result-embed-code">
                        &lt;div class="col-sm-12 col-xs-12"&gt;<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &lt;script src="{%= o.quiz_embed%}"&gt;&lt;/script&gt;<br/>
                        &lt;/div&gt;
                    </div>

                    <div class="col-sm-12 col-xs-12 no-padding margin-10">
                        <b>Tips:</b><br/>
                        Embed works well on sites that support bootstrap interface. In your website should avoid using the following class:
                        <ul>
                            <li>quiz-container-main</li>
                            <li>quiz-container-title</li>
                            <li>quiz-container-start</li>
                            <li>quiz-button-start</li>
                            <li>quiz-container-question</li>
                            <li>quiz-container-answer</li>
                            <li>quiz_label_check</li>
                            <li>quiz_input_check</li>
                            <li>quiz-container-pagination</li>
                            <li>quiz-container-pagination-next</li>
                            <li>quiz-container-pagination-previous</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
@endsection
        

        
