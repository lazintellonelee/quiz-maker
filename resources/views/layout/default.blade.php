<!DOCTYPE html>
<html>
    <head>
        <title>Quiz Maker</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="base-url" content="{{ url() }}">
        
        <link rel="stylesheet" type="text/css" href="{{ url('public/plugins/bootstrap/css/bootstrap.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('public/plugins/bootstrap/css/bootstrap-theme.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('public/plugins/font-awesome-4.4.0/css/font-awesome.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('public/css/style.css') }}" />
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        
        @yield('css')
    </head>
    <body>
        @include('partial/nav-home')
        @include('partial/modal-signup')
        <div class="container-fluid">
            <div class="row">
                @yield('content')
            </div>
        </div>
        
        <footer class="footer">
            <div class="container text-center">
                <a href="#">About</a>
                
                <a href="#">Contact</a>
                
                <a href="#">Help</a>
            </div>
        </footer>
        
        <script type="text/javascript" src="{{ url('public/plugins/jquery/jquery-2.1.4.js') }}"></script>
        <script type="text/javascript" src="{{ url('public/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ url('public/js/home-script.js') }}"></script>
        
        @yield('js')
    </body>
</html>
