<!DOCTYPE html>
<html>
    <head>
        <title>Quiz Embed</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="{{ url('public/plugins/bootstrap/css/bootstrap.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('public/plugins/bootstrap/css/bootstrap-theme.min.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('public/plugins/jquery-ui/css/custom-theme/jquery-ui-1.10.3.custom.css') }}" />
        <link rel="stylesheet" type="text/css" href="{{ url('public/plugins/font-awesome-4.4.0/css/font-awesome.min.css') }}" />
    </head>
    
    <body>
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-3 col-sm-6 col-xs-12">
                    <script src="{{ url('public/js/embed/quiz-maker-embed.js?q=1;sk=ce67a10596c44da37545deccea0b6d21;bootstrap=1') }}"></script>
<!--                    <script src="http://quizmaker.gutek.com.vn/public/js/embed/quiz-maker.quiz-embed.js?q=3;sk=5d16a25ba847d2adad501b9e738df198"></script>-->
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript" src="{{ url('public/plugins/jquery/jquery-2.1.4.js') }}"></script>
    <script type="text/javascript" src="{{ url('public/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
</html>