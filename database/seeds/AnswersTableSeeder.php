<?php

use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $data = array(
        array(
            'question_id'       => 1,
            'answer_content'    => 'Accepted',
            'answer_score'      => 1,
            'answer_index'      => 0
        ),
        array(
            'question_id'       => 1,
            'answer_content'    => 'Excepted',
            'answer_score'      => 0,
            'answer_index'      => 1
        ),
        array(
            'question_id'       => 2,
            'answer_content'    => 'Adverse',
            'answer_score'      => 0,
            'answer_index'      => 0
        ),
        array(
            'question_id'       => 2,
            'answer_content'    => 'Averse',
            'answer_score'      => 1,
            'answer_index'      => 1
        ),
        array(
            'question_id'       => 3,
            'answer_content'    => 'Advise',
            'answer_score'      => 0,
            'answer_index'      => 0
        ),
        array(
            'question_id'       => 3,
            'answer_content'    => 'Advice',
            'answer_score'      => 1,
            'answer_index'      => 1
        ),
        array(
            'question_id'       => 4,
            'answer_content'    => 'Effect',
            'answer_score'      => 0,
            'answer_index'      => 0
        ),
        array(
            'question_id'       => 4,
            'answer_content'    => 'Affect',
            'answer_score'      => 1,
            'answer_index'      => 1
        ),
        array(
            'question_id'       => 5,
            'answer_content'    => 'Altogether',
            'answer_score'      => 0,
            'answer_index'      => 0
        ),
        array(
            'question_id'       => 5,
            'answer_content'    => 'All together',
            'answer_score'      => 1,
            'answer_index'      => 1
        ),
        array(
            'question_id'       => 6,
            'answer_content'    => 'Apprised',
            'answer_score'      => 1,
            'answer_index'      => 0
        ),
        array(
            'question_id'       => 6,
            'answer_content'    => 'Appraised',
            'answer_score'      => 0,
            'answer_index'      => 1
        )
    );
    
    public function run()
    {
        DB::table('tbl_answers')->insert($this->data);
    }
}
