<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $data = array(
        array(
            'quiz_id'           => 1,
            'question_content'  => 'She _____ that Jack would never eat broccoli again.',
            'question_type'     => 'select_one',
            'question_display'  => 2,
            'question_index'    => 0
        ),
        array(
            'quiz_id'           => 1,
            'question_content'  => 'I wouldn’t be _____ to having another slice of pie.',
            'question_type'     => 'select_one',
            'question_display'  => 2,
            'question_index'    => 1
        ),
        array(
            'quiz_id'           => 1,
            'question_content'  => 'Do you have any _____ for a first-time spelunker?',
            'question_type'     => 'select_one',
            'question_display'  => 2,
            'question_index'    => 2
        ),
        array(
            'quiz_id'           => 1,
            'question_content'  => 'Will being around a puppy _____ your allergies?',
            'question_type'     => 'select_one',
            'question_display'  => 2,
            'question_index'    => 3
        ),
        array(
            'quiz_id'           => 1,
            'question_content'  => 'It’s so great to have the whole gang here _____!',
            'question_type'     => 'select_one',
            'question_display'  => 2,
            'question_index'    => 4
        ),
        array(
            'quiz_id'           => 1,
            'question_content'  => 'I just ____ Joey of the fact that his iguana is no longer welcome here.',
            'question_type'     => 'select_one',
            'question_display'  => 2,
            'question_index'    => 5
        )
    );
    
    public function run()
    {
        DB::table('tbl_questions')->insert($this->data);
    }
}
