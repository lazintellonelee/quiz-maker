<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class QuizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $data = array(
        array(
            'quiz_category'     => 'quiz_question',
            'quiz_title'        => 'Trắc nghiệm 6 cặp từ tiếng Anh hay nhầm lẫn',
            'quiz_type'         => 'step_by_step',
            'quiz_force'        => 1,
            'quiz_time_out'     => null,
            'quiz_one_time'     => 0,
            'quiz_created_by'   => 0,
            'quiz_secret_key'   => 'ce67a10596c44da37545deccea0b6d21'
        )
    );

    public function run()
    {
        DB::table('tbl_quizes')->insert($this->data);
    }
}
