<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_questions', function (Blueprint $table) {
            $table->bigIncrements('question_id');
            $table->integer('quiz_id')->default(0);
            $table->text('question_content')->nullable();
            $table->enum('question_type', array('select_one', 'multi_choice'))->default('select_one');
            $table->smallInteger('question_display')->default(1);
            $table->integer('question_index')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_questions');
    }
}
