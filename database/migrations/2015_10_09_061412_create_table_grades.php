<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGrades extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_grades', function (Blueprint $table) {
            $table->bigIncrements('grade_id');
            $table->integer('quiz_id')->unsigned()->default(0);
            $table->string('grade_title')->nullable();
            $table->text('grade_description')->nullable();
            $table->enum('grade_type', array('score', 'percent'))->default('score');
            $table->integer('grade_from')->unsigned()->default(0);
            $table->integer('grade_to')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_grades');
    }
}
