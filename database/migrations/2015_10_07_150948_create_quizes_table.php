<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_quizes', function(Blueprint $table){
            $table->increments('quiz_id');
            $table->enum('quiz_category', array('quiz_question','survey_poll'));
            $table->string('quiz_title')->nullable();
            $table->enum('quiz_type', array('step_by_step', 'do_all'))->default('step_by_step');
            $table->tinyInteger('quiz_force')->default(0);
            $table->tinyInteger('quiz_time_out')->nullable();
            $table->tinyInteger('quiz_one_time')->default(0);
            $table->integer('quiz_created_by')->default(0);
            $table->string('quiz_secret_key')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_quizes');
    }
}
